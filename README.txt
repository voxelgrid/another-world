-> Depedencies:
	* CMake 2.8      (for project generation)
	* Boost 1.47     (for regex manipulation)
	* SFML 1.6       (for event handling)
	* Assimp         (for mesh loading)
	* OpenIL         (for image loading)
	* GLEW           (for OpenGL extensions)

-> How to compile:
	1. Create a directory 'build',
	2. Open a prompt:
		a. cd build
		b. cmake .. -G"Visual Studio 10"   (for example)
	3. Open the solution,
	4. Build the project by pressing F7
