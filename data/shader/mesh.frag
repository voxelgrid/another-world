/*
 * semantic.shader
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, packages.
#include <common.pkg>
#include <material.pkg>
#include <light.pkg>

// Input from vertex shader.
smooth in vec4 v_position;
smooth in vec3 v_normal;

// Output.
out vec4 color;

void main()
{
  vec4 shading;
  for (int i = 0; i < NB_MAX_LIGHT; ++i)
    if (lights.slot[i].enabled != 0)
      shading += compute_light(i, material.diffuse, v_position, normalize(v_normal));

  color = shading;
}
