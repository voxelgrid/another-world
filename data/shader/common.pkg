/*
 * common.pkg
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Required version: 3.3
#version 330

// Enable debug mode.
#pragma debug(on)

// MVP Matrices.
layout(std140) uniform Matrices
{
  mat4 projection;
  mat4 modelview;
  mat4 mvp;
  mat4 view;
  mat4 inverse_view;
  mat4 normal;
  mat4 model;
} matrices;

/// Convert a position from object space to eye space.
vec4 object_to_eye(vec4 vec) { return matrices.modelview * vec; }
/// Convert a position from eye space to clip space.
vec4 eye_to_clip(vec4 vec) { return matrices.projection * vec; }
/// Convert a position from object space to clip space.
vec4 object_to_clip(vec4 vec) { return matrices.mvp * vec; }

/// Convert a normal from object space to eye space.
vec4 normal_eye_space(vec4 vec)
{
  return matrices.normal * vec;
}

// Included.
#define COMMON_PACKAGE_INCLUDED
