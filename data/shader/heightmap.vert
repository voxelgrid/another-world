/*
 * heightmap.vert
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, packages.
#include <common.pkg>
#include <material.pkg>
#include <light.pkg>

// Vertex Input.
layout(location = 0) in vec4 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoord;

// Output.
smooth out float v_altitude;
smooth out vec4 v_position;
smooth out vec3 v_normal;
smooth out vec2 v_texcoord;

void main()
{
  v_altitude  = in_position.y;
  v_position  = object_to_eye(in_position);
  v_position.xyz /= v_position.w;
  v_position.w    = 1;

  v_normal    = normalize(matrices.normal * vec4(in_normal, 0.0)).xyz;
  v_texcoord  = in_texcoord;
  gl_Position = eye_to_clip(v_position);
}
