/*
 * water.frag
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, packages.
#include <common.pkg>
#include <material.pkg>
#include <light.pkg>

// Input from application.
uniform float     in_xwave_length;
uniform float     in_xwave_height;
uniform sampler2D in_reflexion;
uniform sampler2D in_refraction;
uniform sampler2D in_normalmap;
uniform vec3 in_cp;

// Input from vertex shader.
smooth in vec4 v_position;
smooth in vec3 v_normal;
smooth in vec2 v_texcoord;
smooth in vec4 v_refrpos;
smooth in vec4 v_tmp;

// Cam position.
smooth in vec3 v_campos;

// Output.
out vec4 color;

void main()
{
  vec2 projected_texcoord;
  projected_texcoord.s = (v_tmp.x / v_tmp.w) * 0.5f + 0.5;
  projected_texcoord.t = (v_tmp.y / v_tmp.w) * 0.5f + 0.5;
  
  vec2 projected_refr;
  projected_refr.s = (v_refrpos.x / v_refrpos.w) * 0.5f + 0.5;
  projected_refr.t = (v_refrpos.y / v_refrpos.w) * 0.5f + 0.5;

  vec4 refl_color = texture(in_reflexion, projected_texcoord);
  vec4 refr_color = texture(in_refraction, projected_refr);

  vec4 shading;
  for (int i = 0; i < NB_MAX_LIGHT; ++i)
    if (lights.slot[i].enabled != 0)
      shading += compute_light(i, material.diffuse, v_position, v_normal);
      
  // Compute fresnel.
  vec3 pos = normalize(in_cp - v_position.xyz);
  vec3 nor = vec3(0,1,0);
  float k = dot(pos, nor);
  
  // Mix.
  color = material.diffuse * mix(shading, mix(refl_color,refr_color, k), 0.8);
  //color = material.diffuse * mix(refl_color, refr_color, k);
}
