/*
 * semantic.shader
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, packages.
#include <common.pkg>
#include <material.pkg>
#include <light.pkg>

// Input from application.
uniform bool      in_enable_clipping;
uniform bool      in_clip_bottom;
uniform sampler2D in_texture;

// Input from vertex shader.
smooth in float v_altitude;
smooth in vec4 v_position;
smooth in vec2 v_texcoord;
smooth in vec3 v_normal;

// Output.
out vec4 color;

void main()
{
  // Discard ?
  if (in_enable_clipping)
  {
    if (in_clip_bottom && v_altitude < -28.5)
      discard;
    if (!in_clip_bottom && v_altitude > -20.5)
      discard;
  }

  // Texture color.
	color = texture(in_texture, v_texcoord);

  vec4 shading;
  for (int i = 0; i < NB_MAX_LIGHT; ++i)
    if (lights.slot[i].enabled != 0)
      shading += compute_light(i, color, v_position, v_normal);

  color = shading;
}
