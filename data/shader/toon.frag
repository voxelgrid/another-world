/*
 * toon.shader
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, packages.
#include <common.pkg>
#include <material.pkg>
#include <light.pkg>

// Input from vertex shader.
smooth in vec4 v_position;
smooth in vec3 v_normal;

// Output.
out vec4 color;

vec4 toon_directional(int id, vec4 color, vec4 position_eye, vec3 normal_eye)
{
  // Light's direction must be in the eye space.
  vec4 light_pos = matrices.view * vec4(lights.slot[id].position.xyz, 1.0);
  vec3 light_dir = light_pos.xyz / light_pos.w;
  light_dir      = normalize(light_dir);
  
  // Intensity.
  float intensity = dot(light_dir, normalize(normal_eye));
  
  // Toon.
  float factor = 1.0;
  if      ( intensity > 0.95 ) factor = 0.7;
  else if ( intensity > 0.5  ) factor = 0.5;
  else if ( intensity > 0.25 ) factor = 0.4;

  return color * vec4(factor, factor, factor, 1.0) * lights.slot[id].diffuse;
}

vec4 toon_spot(int id, vec4 color, vec4 vertex_eye, vec3 normal_eye)
{
  // Light's position must be in the eye space.
  vec4 tmp = (matrices.view * lights.slot[id].position);
  vec3 light_pos = tmp.xyz / tmp.w;

  // Compute it's direction and it's distance.
  vec3  vertex_to_light = light_pos - vertex_eye.xyz;
  float distance        = length(vertex_to_light);
  vec3  light_dir       = normalize(vertex_to_light);

  // Spot's direction must be in the eye space.
  vec4 spot_pos = matrices.view * vec4(lights.slot[id].spot_direction.xyz, 0.0);
  vec3 spot_dir = normalize(spot_pos.xyz);

  // Spot dot product with the light direction.
  float spotDot = dot(-light_dir, spot_dir);
  float angle   = acos(spotDot);
  float cutoff  = radians(lights.slot[id].spot_cutoff);

  // In the cutoff ?
  float spot_att = 0.f;
  if (angle < cutoff)
    spot_att = pow(spotDot, 20.0);

  // Compute the intensity.
  float nDotVP = max(0.f, dot(normalize(normal_eye), light_dir));

  // Attenuation.
  float att = 1.f / (lights.slot[id].constant_attenuation +
                     lights.slot[id].linear_attenuation * distance +
                     lights.slot[id].quadratic_attenuation * distance * distance);
  att *= spot_att;

  // Intensity.
  float intensity = nDotVP;
  
  // Toon.
  float factor = 1.0;
  if      ( intensity > 0.95 ) factor = 0.7;
  else if ( intensity > 0.5  ) factor = 0.5;
  else if ( intensity > 0.25 ) factor = 0.4;
  else
    factor = 0.2;

  return color * vec4(factor, factor, factor, 1.0) * lights.slot[id].diffuse * att;
}

vec4 toon_point(int id, vec4 color, vec4 position_eye, vec3 normal_eye)
{
  return vec4(0.0, 1.0, 0.0, 1.0);
}

vec4 toon_shading(int id, vec4 color, vec4 position_eye, vec3 normal_eye)
{
  if (is_directional(id))
    return toon_directional(id, color, position_eye, normal_eye);
  else if (is_spot(id))
    return toon_spot(id, color, position_eye, normal_eye);
  else
    return toon_point(id, color, position_eye, normal_eye);
}

void main()
{
  // Toon.
  vec4 shading = vec4(0.0, 0.0, 0.0, 1.0);
  for (int i = 0; i < NB_MAX_LIGHT; ++i)
    if (lights.slot[i].enabled != 0)
      shading += toon_shading(i, material.diffuse, v_position, v_normal);

  color = shading;
  color.a = 0.5;
}
