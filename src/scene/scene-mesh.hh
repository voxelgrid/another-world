/*
 * scene/scene-mesh.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_SCENE_MESH_HH
#define DEF_LO12_SCENE_MESH_HH

// Includes, project.
#include <scene/scene-entity.hh>

namespace lo12
{
  class scene_mesh
    : public scene_entity
  {
    public:
      /// Ctor of scene_mesh.
      scene_mesh();

      /// Render the mesh.
      void draw(renderer&);

      /// Get the rotation angle.
      float angle() const;

      /// Get the rotation axis.
      glm::vec3 rotation_axis() const;

      /// Get the position.
      glm::vec3 position() const;
      
      /// Get the scale.
      glm::vec3 scale() const;

      /// Set the position.
      void set_position(glm::vec3 const&);

      /// Set the rotation.
      void set_rotation(float, glm::vec3 const&);

      /// Set the scale.
      void set_scale(glm::vec3 const&);

    private:
      // Position.
      glm::vec3 m_position;
      float     m_angle;
      glm::vec3 m_rotation;
      glm::vec3 m_scale;
  };
}

#endif /* end of include guard: DEF_LO12_SCENE_MESH_HH */
