/*
 * scene/forest.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <common/debug.hh>
#include <common/path.hh>
#include <core/renderer.hh>
#include <core/semantic.hh>
#include <scene/forest.hh>
// Includes, standard.
#include <cassert>

namespace lo12
{
  forest::forest()
    : scene_entity("forest")
    , m_positions(0)
  {
    // Initialize the shader.
    internal_shader().load_from_disk(path::ShaderPath + "tree.vert",
                                     path::ShaderPath + "tree.frag");
    internal_shader().enable_block(shader_block::lights);
    internal_shader().enable_block(shader_block::material);
    internal_shader().enable_block(shader_block::matrices);
    // Initialize the mesh.
    internal_mesh().load_from_disk(path::MeshPath + "billboard-tree.obj",
                                   mesh_option::gen_texcoord);
    // Load the texture.
    m_texture.load_from_disk(path::TexturePath + "tree0.png");
    // Send the texture.
    internal_shader().send_tex("in_texture", texture_slot::texture0);
  }

  void
  forest::add_tree(glm::vec3 const& position)
  {
    m_positions.push_back(position);
  }

  void
  forest::draw(renderer& engine)
  {
    // Enable blending.
    glEnable(GL_BLEND);
    glDepthMask(GL_FALSE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // Attach the texture.
    engine.set_texture(texture_slot::texture0, m_texture);


    // Draw all the trees with their positions.
    for (size_t i = 0; i < m_positions.size(); ++i)
    {
      // Place the tree.
      engine.model.push(glm::mat4(1.f));
      engine.model.translate(m_positions[i].x,
                             m_positions[i].y,
                             m_positions[i].z);

      // Save the view.
      glm::mat4 view = engine.view.top();
      engine.view.push();
      // Get the camera position.
      glm::vec3 cam = glm::vec3(view[3]);
      // Get the object position.
      glm::mat4 tmp = engine.view.top() * engine.model.top();
      glm::mat3 o = glm::mat3(tmp);
      glm::vec3 obj = cam + glm::transpose(o) * glm::vec3(tmp[3]);
      // Compute the projection.
      glm::vec3 obj_cam_proj = cam - obj;
      obj_cam_proj.y = 0.f;
      // Some computation.
      obj_cam_proj = glm::normalize(obj_cam_proj);
      glm::vec3 n = glm::cross(glm::vec3(0.f, 0.f, 1.f), obj_cam_proj);
      float angle = glm::dot(glm::vec3(0.f, 0.f, 1.f), obj_cam_proj);
      // Rotation.
      if (std::abs(angle) < 0.999)
        engine.model.rotate(std::acos(angle) * (180.f/3.14f), n.x, n.y, n.z);
      // Scale.
      engine.model.scale(3.f, 5.f, 3.f);
      engine.model.rotate(90.f, 0.f, -1.f, 0.f);

      // Draw the tree.
      engine.process(internal_shader(), internal_mesh());
      // Remove current model.
      engine.model.pop();
      engine.view.pop();
    }

    // Disable blending.
    glDisable(GL_BLEND);
    glDepthMask(GL_TRUE);
  }

  void
  forest::remove(size_t index)
  {
    // Check args.
    assert(index < m_positions.size());
    // Remove the tree.
    m_positions.erase(m_positions.begin() + index);
  }

  size_t
  forest::size() const
  {
    return m_positions.size();
  }
}
