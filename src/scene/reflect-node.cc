/*
 * scene/reflect-node.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <common/path.hh>
#include <core/renderer.hh>
#include <scene/reflect-node.hh>

namespace lo12
{
  reflect_node::reflect_node()
    : effect_node()
    , m_texture(NULL)
  {
    load_from_disk(path::ShaderPath + "reflect.vert",
                   path::ShaderPath + "reflect.frag");
    m_shader.enable_block(shader_block::lights);
    m_shader.send_tex("in_texture", texture_slot::skybox);
  }

  reflect_node::~reflect_node()
  {}

  void
  reflect_node::set_texture(cubemap const* texture)
  {
    m_texture = texture;
  }

  void
  reflect_node::on_drawn_before(renderer& engine)
  {
    // Enable the texture.
    engine.set_texture(texture_slot::skybox, *m_texture);
  }

  void
  reflect_node::on_drawn_after(renderer& engine)
  {}
}
