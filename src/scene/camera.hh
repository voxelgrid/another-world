/*
 * scene/camera.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_CAMERA_HH
#define DEF_LO12_CAMERA_HH

// Includes, project.
#include <common/math.hh>

namespace lo12
{
  struct move_state
  {
    /// Ctor of move_state.
    move_state();

    bool forward;      ///< Move forward.
    bool backward;     ///< Move backward.
    bool strafe_left;  ///< Mofe left.
    bool strafe_right; ///< Move right.
    bool down;         ///< Move down.
    bool up;           ///< Move up.
    int  mouse_dx;     ///< Mouse dx.
    int  mouse_dy;     ///< Mouse dy.
  };

  class camera
  {
   public:
      /// Ctor of Camera.
      camera(glm::vec3 const&, glm::vec3 const&, glm::vec3 const&);

      /// Animate the camera.
      void animate(float);

      /// Configure the perspective parameters.
      void configure_perspective(float, float, float, float);

      /// Get the forward vector.
      glm::vec3 forward() const;

      /// Get the position.
      glm::vec3 position() const;

      /// Get the target.
      glm::vec3 target() const;

      /// Change the camera's sensitivity.
      void set_sensitivity(float);

      /// Change the camera's speed.
      void set_speed(float);

      /// Get the perspective matrix.
      glm::mat4 perspective_matrix() const;

      /// Get the view matrix.
      glm::mat4 view_matrix() const;

      /// Get the move state.
      move_state& state();

  private:
      /// Update the vectors from angles.
      void update_vectors();

  private:
      // Speed and sensitivity.
      float m_speed;
      float m_sensitivity;

      // Move state.
      move_state m_state;

      // Vectors.
      glm::vec3 m_forward;
      glm::vec3 m_left;
      glm::vec3 m_position;
      glm::vec3 m_target;
      glm::vec3 m_up;

      // Angles.
      float m_pitch;
      float m_yaw;

      // Perspective configuration.
      float m_fov;
      float m_ratio;
      float m_znear;
      float m_zfar;
  };
}

#endif /* end of include guard: DEF_LO12_CAMERA_HH */