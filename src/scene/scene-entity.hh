/*
 * scene/scene-entity.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_SCENE_ENTITY_HH
#define DEF_LO12_SCENE_ENTITY_HH

// Includes, project.
#include <core/mesh.hh>
#include <core/shader.hh>
// Includes, standard.
#include <string>

namespace lo12
{
  // Forward declarations.
  class renderer;

  class scene_entity
  {
    public:
      /// Ctor of scene_entity.
      scene_entity(std::string const&);

      /// Dtor of scene_entity.
      virtual
      ~scene_entity();

      /// Render the entity.
      virtual
      void draw(renderer&) = 0;

      /// Get the name.
      std::string name() const;

      /// Get the mesh.
      mesh& internal_mesh() { return m_mesh; }
      mesh const& internal_mesh() const { return m_mesh; }

      /// Get the shader.
      shader& internal_shader() { return m_shader; }
      shader const& internal_shader() const { return m_shader; }

    protected:
      // Allow research in the scene graph by name.
      std::string m_name;
      // Core objects.
      mesh   m_mesh;
      shader m_shader;
  };
}

#endif /* end of include guard: DEF_LO12_SCENE_ENTITY_HH */
