/*
 * scene/sun.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <common/path.hh>
#include <core/light.hh>
#include <core/renderer.hh>
#include <scene/sun.hh>
#include <scene/skybox.hh>
// Includes, standard.
#include <iostream>

namespace lo12
{
  // First configuration.
  glm::vec4 const SunFirstPosition(1000.f, 1000.f, 0.f, 1.0);
  glm::vec3 const SunScale(50.f, 50.f, 50.f);
  // Configuration.
  float const AngularSpeed = 100.f;

  // Different 
  sun::sun()
    : scene_entity("sun")
    , m_light(NULL)
    , m_sky(NULL)
    , m_position(SunFirstPosition)
  {
    // Initialize the shader, loading.
    m_shader.load_from_disk(path::ShaderPath + "sun.vert",
                            path::ShaderPath + "sun.frag");
    // Initialize some blocks.
    m_shader.enable_block(shader_block::material);
    m_shader.enable_block(shader_block::matrices);
    // Load a mesh.
    m_mesh.load_from_disk(path::MeshPath + "sphere.3ds");
    // Set the sun color.
    m_mesh.material.diffuse = glm::vec4(1.f, 0.f, 0.f, 1.f);
  }

  void
  sun::draw(renderer& engine)
  {
    engine.model.push(glm::mat4(1.f));
    engine.model.translate(m_position.x, m_position.y, m_position.z);
    engine.model.scale(SunScale.x, SunScale.y, SunScale.z);

    engine.process(m_shader, m_mesh);

    engine.model.pop();
  }

  void
  sun::set_light(light* ptr)
  {
    // Check args.
    assert(ptr);
    // Update the light.
    m_light = ptr;
  }

  void
  sun::set_sky(skybox* sky)
  {
    // Check args.
    assert(sky);

    m_sky = sky;
  }

  void
  sun::update(float dt)
  {
    // Check internal state.
    assert(m_light);
    assert(m_sky);

    // Compute the rotation.
    glm::mat4 rotate = glm::rotate(glm::mat4(1.f),
                                   AngularSpeed * dt,
                                   glm::vec3(1.f, 0.f, 1.f));
    
    // Rotate the sun.
    m_position = rotate * m_position;
    // Update the light.
    m_light->position = m_position;
    m_light->position.w = 0.f;

    // Update the color of the light according it's cycle.
    // TODO: this is ugly, interpolate the values.
    if (m_position.y > 800.f)
      m_light->diffuse = glm::vec4(1.f, 1.f, 1.f, 1.f);
    else if (m_position.y > 700.f)
      m_light->diffuse = glm::vec4(0.9f, 0.9f, 0.9f, 0.8f);
    else if (m_position.y > 600.f)
      m_light->diffuse = glm::vec4(0.8f, 0.8f, 0.8f, 0.8f);
    else if (m_position.y > 500.f)
      m_light->diffuse = glm::vec4(0.7f, 0.7f, 0.7f, 0.7f);
    else if (m_position.y > 400.f)
      m_light->diffuse = glm::vec4(0.6f, 0.6f, 0.6f, 0.6f);
    else if (m_position.y > 300.f)
      m_light->diffuse = glm::vec4(0.5f, 0.5f, 0.5f, 0.5f);
    else if (m_position.y > 200.f)
      m_light->diffuse = glm::vec4(0.4f, 0.4f, 0.4f, 0.4f);
    else if (m_position.y > 100.f)
      m_light->diffuse = glm::vec4(0.3f, 0.3f, 0.3f, 0.3f);
    else if (m_position.y > 0.f)
      m_light->diffuse = glm::vec4(0.2f, 0.2f, 0.2f, 0.1f);
    else
      m_light->diffuse = glm::vec4(0.0f, 0.0f, 0.0f, 1.f);

    // Update the sky.
    m_sky->set_intensity(m_light->diffuse.r);
  }
}
