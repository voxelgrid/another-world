/*
 * scene/heightmap.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_HEIGHTMAP_HH
#define DEF_LO12_HEIGHTMAP_HH

// Includes, project.
#include <core/texture2d.hh>
#include <scene/scene-entity.hh>
#include <tools/image-loader.hh>
// Includes, standard.
#include <cstdint>
#include <string>

namespace lo12
{
  // Default padding.
  size_t const DefaultPadding = 5;

  class heightmap
    : public scene_entity
  {
    public:
      /// Ctor of heightmap.
      heightmap();

      /// Dtor of heightmap.
      ~heightmap();

      /// Load from disk.
      void load_from_disk(std::string const&,
                          std::string const&,
                          size_t = DefaultPadding);

      /// Draw the heightmap.
      void draw(renderer&);

      /// Set a scale.
      void set_scale(float, float);

      /// Get the size.
      float size() const;

    private:
      /// Allocate the internal memory.
      void allocate_memory();

      /// Generate the vertices.
      void generate_vertices(image_array const&, size_t);

      /// Generate the heightmap's normals.
      void generate_normals();

      /// Generate the indices.
      void generate_indices();

      /// Send the CPU data to the GPU.
      void send_to_gpu();

    private:
      texture2d m_texture;
      glm::vec3 m_scale; 

      size_t m_width;
      size_t m_height;
      size_t m_precision;

      std::vector<glm::vec3> m_position;
      std::vector<glm::vec2> m_texcoord;
      std::vector<glm::vec3> m_normal;
      std::vector<size_t>    m_index;
  };
}

#endif /* end of include guard: DEF_LO12_HEIGHTMAP_HH */
