/*
 * scene/scene.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_SCENE_HH
#define DEF_LO12_SCENE_HH

// Includes, project.
#include <core/light.hh>
#include <core/framebuffer.hh>
#include <core/renderer.hh>
#include <scene/camera.hh>
#include <scene/forest.hh>
#include <scene/heightmap.hh>
#include <scene/reflect-node.hh>
#include <scene/scene-mesh.hh>
#include <scene/skybox.hh>
#include <scene/sun.hh>
#include <scene/toon-node.hh>
#include <scene/water.hh>

namespace lo12
{
  class scene
  {
    public:
      /// Ctor of scene.
      scene();

      /// Dtor of scene.
      ~scene();

      /// Get the camera.
      camera& get_camera();

      /// Get the engine.
      renderer& engine() { return m_engine; }
      renderer const& engine() const { return m_engine; }

      /// Draw the scene.
      void render();

      /// Update the scene.
      void update(float);

    private:
      /// Initialize the scene.
      void initialize();

      /// Initialize the meshes.
      void initialize_meshes();

      /// Final pass.
      void final_pass();

      /// Render the objects, not the water.
      void render_objects();

      /// Water pass.
      void water_pass();

    private:
      // Engine.
      renderer m_engine;

      // Camera.
      camera m_camera;

      // Objects.
      heightmap    m_terrain;
      forest       m_forest;
      scene_mesh   m_teapot;
      scene_mesh   m_turtle;
      scene_mesh   m_monkey;
      skybox       m_sky;
      sun          m_sun;

      // Water.
      water m_water;

      // Render traget for the water rendering.
      framebuffer m_water_target;

      // Efects.
      reflect_node m_reflect;
      toon_node    m_toon;
  };
}

#endif /* end of include guard: DEF_LO12_SCENE_HH */
