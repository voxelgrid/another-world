/*
 * scene/water.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_WATER_HH
#define DEF_LO12_WATER_HH

// Includes, project.
#include <core/texture2d.hh>
#include <scene/scene-entity.hh>

namespace lo12
{
  // Forward declarations.
  class camera;
  class framebuffer;
  class renderer;

  class water
    : public scene_entity
  {
    public:
      /// Ctor of water.
      water();

      /// Dtor of water.
      ~water();

      /// Attach the texture to the render target.
      void configure_target(framebuffer&, bool);

      /// Refresh the view matrix.
      glm::mat4 compute_view(camera const&);

      /// Render the water.
      void draw(renderer&);
 
      /// Set the plane height.
      void set_altitude(float);

      /// Set the plane dimension.
      void set_size(float);

      /// Update the time for wind.
      void update(float dt);

    private:
      // Current time.
      float m_time;
      // Dimension and position.
      float     m_altitude;
      float     m_size;
      // Textures.
      texture2d m_normalmap;
      texture2d m_reflexion;
      texture2d m_refraction;
      // Last view computed.
      glm::mat4 m_view;
  };
}

#endif /* end of include guard: DEF_LO12_WATER_HH */
