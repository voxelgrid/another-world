/*
 * scene/water.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <common/config.hh>
#include <common/debug.hh>
#include <common/path.hh>
#include <core/framebuffer.hh>
#include <core/renderer.hh>
#include <scene/camera.hh>
#include <scene/water.hh>
// Includes, standard.
#include <cassert>

namespace lo12
{
  water::water()
    : scene_entity("water")
    , m_time(0.f)
    , m_altitude(0.f)
    , m_size(1.f)
    , m_normalmap()
    , m_reflexion()
    , m_refraction()
    , m_view(1.f)
  {
    // Load a plane.
    internal_mesh().load_from_disk(path::MeshPath + "plane.obj",
                                   mesh_option::gen_texcoord);
    // Configure the material.
    internal_mesh().material.diffuse = glm::vec4(0.6f, 0.68f, 0.97f, 0.f);
    // Load the water shader.
    internal_shader().load_from_disk(path::ShaderPath + "water.vert",
                                     path::ShaderPath + "water.frag");
    // Enable some UBO.
    internal_shader().use();
    internal_shader().enable_block(shader_block::lights);
    internal_shader().enable_block(shader_block::material);
    internal_shader().enable_block(shader_block::matrices);
    // Allocate the reflexion.
    m_reflexion.allocate(texture_format::rgba,
                         config::ViewportWidth,
                         config::ViewportHeight);
    // Allocate the refraction.
    m_refraction.allocate(texture_format::rgba,
                          config::ViewportWidth,
                          config::ViewportHeight);
    // Load the normalmap.
    m_normalmap.load_from_disk(path::TexturePath + "water-normalmap.png");
    // Send the textures.
    internal_shader().send_tex("in_reflexion", texture_slot::texture0);
    internal_shader().send_tex("in_refraction", texture_slot::skybox);
  }

  water::~water()
  {
  }

  void
  water::configure_target(framebuffer& target, bool refraction)
  {
    if (refraction)
      target.attach(framebuffer::color0_slot, m_refraction);
    else 
      target.attach(framebuffer::color0_slot, m_reflexion);
  }

  glm::mat4
  water::compute_view(camera const& user_camera)
  {
    glm::vec3 pos = user_camera.position();
    glm::vec3 tar = user_camera.target();

    pos.y = -pos.y + 2 * m_altitude;
    tar.y = -tar.y + 2 * m_altitude;

    m_view = glm::lookAt(pos, tar, glm::vec3(0.f, 1.f, 0.f));
    return m_view;
  }

  void
  water::draw(renderer& engine)
  {
    // Save matrices.
    engine.model.push(glm::mat4(1.f));
    // Move and scale the center of the axes.
    engine.model.translate(0.f, m_altitude, 0.f);
    engine.model.scale(m_size, 1.f, m_size);

    // Send the view.
    internal_shader().use();
    internal_shader().send_mat4("in_preview", &m_view[0][0]);
    // Set the textures.
    engine.set_texture(texture_slot::texture0, m_reflexion);
    engine.set_texture(texture_slot::skybox, m_refraction);
    // Draw the mesh.
    engine.process(internal_shader(), internal_mesh());
    // Restore matrices.
    engine.model.pop();
  }

  void
  water::set_altitude(float value)
  {
    m_altitude = value;
  }

  void
  water::set_size(float value)
  {
    // Check args.
    assert(value > 0.f);
    // OK.
    m_size = value;
  }

  void
  water::update(float dt)
  {
    // Update.
    m_time += dt;
    // Send to shader.
    //internal_shader().use();
    //internal_shader().send_float("in_xtime", m_time);
  }
}
