/*
 * scene/skybox.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <common/path.hh>
#include <core/renderer.hh>
#include <core/semantic.hh>
#include <scene/skybox.hh>

namespace lo12
{
  // Cube position.
  static size_t const NbVertices = 24;
  static float const CubeVertices[NbVertices] = {
    -1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, +1.0f,
    +1.0f, +1.0f, +1.0f,
    -1.0f, +1.0f, +1.0f,
    -1.0f, -1.0f, -1.0f,
    +1.0f, -1.0f, -1.0f,
    +1.0f, +1.0f, -1.0f,
    -1.0f, +1.0f, -1.0f
  };

  // Cube index.
  static size_t const NbIndices = 36;
  static size_t const CubeIndices[NbIndices] = {
    3, 1, 0,
    3, 2, 1,
    4, 5, 7,
    5, 6, 7,
    7, 2, 3,
    7, 6, 2,
    0, 5, 4,
    0, 1, 5,
    7, 0, 4,
    7, 3, 0,
    2, 5, 1,
    2, 6, 5
  };

  skybox::skybox()
    : scene_entity("sky")
    , m_texture()
  {}

  void
  skybox::load_from_disk(std::string const& dirname, std::string const& ext)
  {
    // Convention over configuration.
    m_texture.load_from_disk(dirname + "posx." + ext, dirname + "negx." + ext,
                             dirname + "posy." + ext, dirname + "negy." + ext,
                             dirname + "posz." + ext, dirname + "negz." + ext);

    // Initialize the sky shader.
    m_shader.load_from_disk(path::ShaderPath + "sky.vert",
                            path::ShaderPath + "sky.frag");
    // Configure the shader.
    m_shader.enable_block(shader_block::matrices);
    m_shader.send_tex("in_cubemap", texture_slot::skybox);
    m_shader.send_float("in_intensity", 1.f);

    geometry& vbo     = m_mesh.geometry;
    index_buffer& ibo = m_mesh.geometry.indices();

    vbo.resize(vertex_attribute::position, NbVertices * sizeof(float));
    vbo.upload(vertex_attribute::position, CubeVertices, NbVertices);
    ibo.resize(sizeof(size_t) * NbIndices);
    ibo.upload(CubeIndices, 0, NbIndices);
  }

  void
  skybox::draw(renderer& engine)
  {
    // Only keep the camera orientation.
    glm::mat4 view = engine.view.top();
    view[3] = glm::vec4(0.f, 0.f, 0.f, 1.f);
    engine.view.push(view);

    // Set the sky texture on the right slot.
    engine.set_texture(texture_slot::skybox, m_texture);

    // Rendering!
    engine.process(m_shader, m_mesh);

    // Restore the view.
    engine.view.pop();
  }

  void
  skybox::set_intensity(float intensity)
  {
    // Check args.
    assert(intensity >= 0.f && intensity <= 1.f);

    // Update the shader.
    m_shader.use();
    m_shader.send_float("in_intensity", intensity);
  }
}
