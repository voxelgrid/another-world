/*
 * scene/heightmap.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <common/path.hh>
#include <core/renderer.hh>
#include <core/semantic.hh>
#include <scene/heightmap.hh>
// Includes, standard.
#include <cassert>

namespace lo12
{
  heightmap::heightmap()
    : scene_entity("heightmap")
    , m_texture()
    , m_scale(1.f, 1.f, 1.f)
    , m_width(0)
    , m_height(0)
    , m_precision(0)
    , m_position(0)
    , m_texcoord(0)
    , m_normal(0)
    , m_index(0)
  {
    // Load the shader.
    internal_shader().load_from_disk(path::ShaderPath + "heightmap.vert",
                                     path::ShaderPath + "heightmap.frag");
    // Use matrices.
    internal_shader().enable_block(shader_block::material);
    internal_shader().enable_block(shader_block::matrices);
    internal_shader().enable_block(shader_block::lights);
    // Send some information.
    internal_shader().send_bool("in_enable_clipping", false);
  }

  heightmap::~heightmap()
  {}

  void
  heightmap::load_from_disk(std::string const& filename,
                            std::string const& texture,
                            size_t padding)
  {
    // Prevent from dividing by zero.
    assert(padding != 0);

    image_array pixels;
    size_t width  = 0;
    size_t height = 0;
    size_t bpp    = 0;

    // Load the texture.
    m_texture.load_from_disk(texture);
    m_shader.send_tex("in_texture", texture_slot::texture0);

    // Load the heightmap.
    static image_loader loader;
    loader.load_from_disk(filename, pixels, width, height, bpp);

    m_width  = width;
    m_height = height;
    m_precision = padding;

    allocate_memory();
    generate_vertices(pixels, bpp);
    generate_indices();
    send_to_gpu();
  }

  void
  heightmap::draw(renderer& engine)
  {
    // Scale the heightmap.
    engine.model.push();
    engine.model.scale(m_scale.x, m_scale.y, m_scale.z);

    // Send the texture and draw the terrain.
    engine.set_texture(texture_slot::texture0, m_texture);
    engine.process(internal_shader(), internal_mesh());

    // Restore the engine matrices.
    engine.model.pop();
  }

  void
  heightmap::set_scale(float base, float height)
  {
    m_scale.x = base;
    m_scale.y = height;
    m_scale.z = base;
  }

  void
  heightmap::allocate_memory()
  {
    // Compute the size of the buffers.
    size_t width  = m_width / m_precision;
    size_t height = m_height / m_precision;
    size_t nb_vertices = width * height;
    size_t nb_index    = (width - 1) * (height - 1);

    // Allocate memory on the CPU.
    m_position.resize(nb_vertices);
    m_texcoord.resize(nb_vertices);
    m_normal.resize(nb_vertices);
    m_index.resize(nb_index * 6);

    // Get the GPU's buffers.
    geometry& vbo = internal_mesh().geometry;
    index_buffer& ibo = vbo.indices();

    // Allocate memory on the GPU.
    vbo.resize(vertex_attribute::position,  nb_vertices * sizeof(glm::vec3));
    vbo.resize(vertex_attribute::texcoord0, nb_vertices * sizeof(glm::vec2));
    vbo.resize(vertex_attribute::normal,    nb_vertices * sizeof(glm::vec3));
    ibo.resize(m_index.size() * sizeof(size_t));
  }

  void
  heightmap::generate_vertices(image_array const& pixels, size_t bpp)
  {
    size_t image_offset = 0;
    size_t array_offset = 0;
    uint8_t gray        = 0;

    size_t middle_x = m_width  / 2;
    size_t middle_y = m_height / 2;

    // First pass: generate the position and the texcoord.
    for (size_t y = 0; y < m_height - m_precision; y += m_precision)
    {
      for (size_t x = 0; x < m_width - m_precision; x += m_precision)
      {
        // Get the pixel offset.
        image_offset = x*bpp + y*m_width*bpp;

        // The gray level is the altitude.
        gray = pixels[image_offset];

        // Center the heightmap.
        m_position[array_offset].x = static_cast<float>(x) - middle_x;
        m_position[array_offset].y = static_cast<float>(gray) - 125;
        m_position[array_offset].z = static_cast<float>(y) - middle_y;

        m_texcoord[array_offset].x = static_cast<float>(x) / m_width;
        m_texcoord[array_offset].y = static_cast<float>(y) / m_height;

        array_offset++;
      }
    }

    // Second pass: generate the normals.
    generate_normals();
  }

  void
  heightmap::generate_normals()
  {
    // Contains the position in the vertex buffer.
    // {0: current}, {1: right}, {2: up}, {3: right-up}.
    size_t n_index[4] = {0};

    size_t width  = m_width  / m_precision;
    size_t height = m_height / m_precision;

    glm::vec3 p0, p1, p2; // Points.
    glm::vec3 v0, v1;     // Triangle vectors.
    glm::vec3 normal;

    // Compute the normals.
    for (size_t y = 0; y < height-1; ++y)
    {
      for (size_t x = 0; x < width-1; ++x)
      {
        // Neighbor's indices.
        n_index[0] = x + width * y;        n_index[1] = (x+1) + width * y;
        n_index[2] = x + width * (y + 1);  n_index[3] = (x+1) + width * (y + 1);


        // First triangle.

        p0 = m_position[n_index[1]];
        p1 = m_position[n_index[0]];
        p2 = m_position[n_index[2]];

        v0 = p1 - p0;
        v1 = p2 - p0;
        
        normal = glm::cross(v0, v1);
        m_normal[n_index[1]] += normal;
        m_normal[n_index[0]] += normal;
        m_normal[n_index[2]] += normal;


        // Second triangle.

        p0 = m_position[n_index[3]];
        p1 = m_position[n_index[1]];
        p2 = m_position[n_index[2]];

        v0 = p1 - p0;
        v1 = p2 - p0;

        normal = glm::cross(v0, v1);
        m_normal[n_index[3]] += normal;
        m_normal[n_index[1]] += normal;
        m_normal[n_index[2]] += normal;
      }
    }

    // Normalize the normals.
    for (size_t i = 0; i < m_normal.size(); ++i)
      m_normal[i] = glm::normalize(m_normal[i]);
  }

  void
  heightmap::generate_indices()
  {
    size_t max_x = ((m_width  - m_precision) / m_precision);
    size_t max_y = ((m_height - m_precision) / m_precision);
    size_t tmp_w = (m_width / m_precision);
    size_t counter = 0;

    // Third pass: generate the indices.
    for (size_t y = 0; y < max_y; ++y)
    {
      for (size_t x = 0; x < max_x; ++x)
      {
        size_t lower_left  = x + y * tmp_w;
        size_t lower_right = (x + 1) + y * tmp_w;
        size_t top_left    = x + (y + 1) * tmp_w;
        size_t top_right   = (x + 1) + (y + 1) * tmp_w;

        m_index[counter++] = lower_right;
        m_index[counter++] = lower_left;
        m_index[counter++] = top_left;

        m_index[counter++] = top_right;
        m_index[counter++] = lower_right;
        m_index[counter++] = top_left;
      }
    }
  }

  float
  heightmap::size() const
  {
    return m_scale.x * m_width;
  }

  void
  heightmap::send_to_gpu()
  {
    // Get the GPU's buffers, memory is allocated.
    geometry& vbo = internal_mesh().geometry;
    index_buffer& ibo = vbo.indices();

    // Send the vertices.
    vbo.upload(vertex_attribute::position,  &m_position[0], m_position.size());
    vbo.upload(vertex_attribute::texcoord0, &m_texcoord[0], m_texcoord.size());
    vbo.upload(vertex_attribute::normal,    &m_normal[0], m_normal.size());

    // Send the indices.
    ibo.upload(&m_index[0], 0, m_index.size());
  }
}
