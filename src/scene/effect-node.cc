/*
 * scene/effect-node.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <core/renderer.hh>
#include <scene/effect-node.hh>

namespace lo12
{
  effect_node::effect_node()
    : scene_entity("effect")
    , m_meshes(0)
  {
  }

  effect_node::~effect_node()
  {
  }

  void
  effect_node::add_mesh(scene_mesh* mesh)
  {
    m_meshes.push_back(mesh);
  }

  void
  effect_node::load_from_disk(std::string const& vert, std::string const& frag)
  {
    m_shader.load_from_disk(vert, frag);
    m_shader.enable_block(shader_block::material);
    m_shader.enable_block(shader_block::matrices);
  }

  void
  effect_node::draw(renderer& engine)
  {
    on_drawn_before(engine);
    for (size_t i = 0; i < m_meshes.size(); ++i)
    {
      // Save the model matrix.
      engine.model.push(glm::mat4(1.f));

      // Move the mesh.
      float     ang = m_meshes[i]->angle();
      glm::vec3 rot = m_meshes[i]->rotation_axis();
      glm::vec3 pos = m_meshes[i]->position();
      glm::vec3 sca = m_meshes[i]->scale();
      // Apply the transformation.
      engine.model.translate(pos.x, pos.y, pos.z);
      if (ang > 0.1f)
        engine.model.rotate(ang, rot.x, rot.y, rot.z);
      engine.model.scale(sca.x, sca.y, sca.z);

      // Rendering.
      engine.process(m_shader, m_meshes[i]->internal_mesh());

      // Restore the matrix.
      engine.model.pop();
    }
    on_drawn_after(engine);
  }
}
