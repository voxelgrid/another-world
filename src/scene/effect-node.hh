/*
 * scene/effect-node.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_EFFECT_NODE_HH
#define DEF_LO12_EFFECT_NODE_HH

// Includes, project.
#include <scene/scene-entity.hh>
#include <scene/scene-mesh.hh>

namespace lo12
{
  class effect_node
    : public scene_entity
  {
    public:
      /// Ctor of effect_node.
      effect_node();

      /// Dtor of effect_node.
      virtual
      ~effect_node();

      /// Add a mesh to the node.
      void add_mesh(scene_mesh*);

      /// Load from disk.
      void load_from_disk(std::string const&, std::string const&);

      /// Initialize the meshes in the node.
      void initialize();

      /// Render the mesh in the node.
      virtual
      void draw(renderer&);

    protected:
      /// Called after the effect has been loaded.
      virtual void on_loaded()
      {}

      /// Called before the effect is drawn.
      virtual void on_drawn_before(renderer& engine)
      {}

      /// Called after the effect is drawn.
      virtual void on_drawn_after(renderer& engine)
      {}

    protected:
      std::vector<scene_mesh*> m_meshes;
  };
}

#endif /* end of include guard: DEF_LO12_EFFECT_NODE_HH */
