/*
 * scene/camera.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, header.
#include <scene/camera.hh>
// Includes, standard.
#include <cmath>

namespace lo12
{
  // Default perspective parameters.
  float const DefaultFOV   = 45.f;
  float const DefaultRatio = 1.f / 3.f;
  float const DefaultZNear = 0.1f;
  float const DefaultZFar  = 100.f;

  move_state::move_state()
    : forward(false)
    , backward(false)
    , strafe_left(false)
    , strafe_right(false)
    , down(false)
    , up(false)
    , mouse_dx(0)
    , mouse_dy(0)
  {
  }

  camera::camera(glm::vec3 const& position,
                 glm::vec3 const& target,
                 glm::vec3 const& up)
    : m_speed(1.f)
    , m_sensitivity(1.f)
    , m_state()
    , m_forward(0.f)
    , m_left(0.f)
    , m_position(position)
    , m_target(target)
    , m_up(up)
    , m_pitch(120.f)
    , m_yaw(-160.f)
    , m_fov(DefaultFOV)
    , m_ratio(DefaultRatio)
    , m_znear(DefaultZNear)
    , m_zfar(DefaultZFar)
  {
    // Update the vectors.
    update_vectors();

    // Initialize the target.
    m_target = m_position + m_forward;
  }

  void
  camera::animate(float dt)
  {
    if (m_state.forward)
      m_position += m_forward * (dt * m_speed);
    if (m_state.backward)
      m_position -= m_forward * (dt * m_speed);

    if (m_state.strafe_left)
      m_position += m_left * (dt * m_speed);
    if (m_state.strafe_right)
      m_position -= m_left * (dt * m_speed);

    if (m_state.down)
      m_position -= m_up;
    if (m_state.up)
      m_position += m_up;

    float dx = static_cast<float>(m_state.mouse_dx);
    float dy = static_cast<float>(m_state.mouse_dy);

    if (m_state.mouse_dx)
      m_yaw += dx * dt * m_sensitivity;
    if (m_state.mouse_dy)
      m_pitch += dy * dt * m_sensitivity;

    // Avoid looping.
    m_pitch = glm::clamp(m_pitch, 1.f, 179.f);

    // Refresh vectors if the mouse has been moved.
    if (m_state.mouse_dx || m_state.mouse_dy)
      update_vectors();

    // Refresh the target.
    m_target = m_position + m_forward;
  }

   void
   camera::configure_perspective(float fov, float ratio,
                                 float znear, float zfar)
   {
     // Pre-condition.
     assert(std::abs(fov)   >= 0.1f);
     assert(std::abs(ratio) >= 1.0f);
     assert(std::abs(znear) >= 0.1f);
     assert(std::abs(zfar)  >  znear);

     m_fov   = fov;
     m_ratio = ratio;
     m_znear = znear;
     m_zfar  = zfar;
   }

  glm::vec3
  camera::forward() const
  {
    return m_forward;
  }

  glm::vec3
  camera::position() const
  {
    return m_position;
  }

  glm::vec3
  camera::target() const
  {
    return m_target;
  }

  void
  camera::set_sensitivity(float value)
  {
    // Check args.
    assert(value > 0.f);

    m_sensitivity = value;
  }

  void
  camera::set_speed(float value)
  {
    // Check args.
    assert(value > 0.f);

    m_speed = value;
  }

  glm::mat4
  camera::perspective_matrix() const
  {
    return glm::perspective(m_fov, m_ratio, m_znear, m_zfar);
  }

  glm::mat4
  camera::view_matrix() const
  {
    return glm::lookAt(m_position, m_target, m_up);
  }

  move_state&
  camera::state()
  {
    return m_state;
  }

  void
  camera::update_vectors()
  {
    float r_prime = std::sin(glm::radians(m_pitch));

    m_forward.x = r_prime * std::cos(glm::radians(m_yaw));
    m_forward.z = r_prime * std::sin(glm::radians(m_yaw));
    m_forward.y = std::cos(glm::radians(m_pitch));

    m_forward = glm::normalize(m_forward);
    m_left    = glm::normalize(glm::cross(m_up, m_forward));
  }
}
