/*
 * scene/scene-mesh.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <core/renderer.hh>
#include <scene/scene-mesh.hh>

namespace lo12
{
  scene_mesh::scene_mesh()
    : scene_entity("mesh")
    , m_position(0.f, 0.f, 0.f)
    , m_angle(0.f)
    , m_rotation(0.f, 0.f, 0.f)
    , m_scale(1.f, 1.f, 1.f)
  {
  }

  void
  scene_mesh::draw(renderer& engine)
  {
    // Set the model matrix.
    engine.model.push();
    engine.model.translate(m_position.x, m_position.y, m_position.z);
    engine.model.scale(m_scale.x, m_scale.y, m_scale.z);
    engine.model.rotate(m_angle, m_rotation.x, m_rotation.y, m_rotation.z);

    // Draw the mesh.
    engine.process(internal_shader(), internal_mesh());

    // Restore the model matrix.
    engine.model.pop();
  }

  float
  scene_mesh::angle() const
  {
    return m_angle;
  }

  glm::vec3
  scene_mesh::rotation_axis() const
  {
    return m_rotation;
  }

  glm::vec3
  scene_mesh::position() const
  {
    return m_position;
  }

  glm::vec3
  scene_mesh::scale() const
  {
    return m_scale;
  }

  void
  scene_mesh::set_position(glm::vec3 const& position)
  {
    m_position = position;
  }

  void
  scene_mesh::set_rotation(float angle, glm::vec3 const& r)
  {
    m_angle = angle;
    m_rotation = r;
  }

  void
  scene_mesh::set_scale(glm::vec3 const& scale)
  {
    m_scale = scale;
  }
}
