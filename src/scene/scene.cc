/*
 * scene/scene.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <common/config.hh>
#include <common/debug.hh>
#include <common/path.hh>
#include <core/renderer.hh>
#include <scene/scene.hh>

namespace lo12
{
  scene::scene()
    : m_engine()
    , m_camera(glm::vec3(5.f, 5.f, 5.f),
               glm::vec3(0.f, 0.f, 0.f),
               glm::vec3(0.f, 1.f, 0.f))
    , m_terrain()
    , m_forest()
    , m_teapot()
    , m_turtle()
    , m_monkey()
    , m_reflect()
    , m_sky()
    , m_sun()
    , m_water()
    , m_water_target()
    , m_toon()
  {
    initialize();
  }

  scene::~scene()
  {}

  camera&
  scene::get_camera()
  {
    return m_camera;
  }

  void
  scene::render()
  {
    // Will fill the reflexion texture.
    water_pass();
    // Will draw everything.
    final_pass();
  }

  void
  scene::update(float dt)
  {
    // Update the sun.
    m_sun.update(dt);
    // Update the water.
    m_water.update(dt);

    // Update torus.
    m_teapot.set_rotation(m_teapot.angle() + 0.1, glm::vec3(0.f, 1.f, 0.f));

    // Update turtle.
    float adeg = m_turtle.angle() + dt * 1000.f;
    float arad = adeg * 3.14 / 180.f;
    m_turtle.set_rotation(adeg, glm::vec3(0.f, -1.f, 0.f));
    m_turtle.set_position(glm::vec3(10.f * sin(-arad), 0.f, 10.f * cos(-arad)));
  }

  void
  scene::initialize()
  {
    // Sky.
    m_sky.load_from_disk(path::CubemapPath + "autumn/", "png");
    // Sun.
    m_sun.set_light(m_engine.get_light(0));
    m_sun.set_sky(&m_sky);

    // Terrain.
    m_terrain.load_from_disk(path::TexturePath + "terrain.gif",
                             path::TexturePath + "terrain-color.jpg");
    m_terrain.set_scale(0.3f, 0.12f);

    // Forest.
    m_forest.add_tree(glm::vec3(-15.f, 8.5f, -30.f));
    m_forest.add_tree(glm::vec3(-10.f, 8.f, -35.f));

    // Load the teapot.
    mesh& teapot_mesh = m_teapot.internal_mesh();
    teapot_mesh.load_from_disk(path::MeshPath + "monkey.obj");
    teapot_mesh.material.specular = glm::vec4(1.f, 1.f, 1.f, 0.f);
    m_teapot.set_scale(glm::vec3(3.f, 3.f, 3.f));
    m_teapot.set_position(glm::vec3(0.f, 1.f, 0.f));

    // Load the turtle.
    mesh&   turtle_mesh   = m_turtle.internal_mesh();
    shader& turtle_shader = m_turtle.internal_shader();
    // Mesh.
    turtle_mesh.load_from_disk(path::MeshPath + "monkey.obj");
    // Shader.
    turtle_shader.load_from_disk(path::ShaderPath + "mesh.vert",
                                 path::ShaderPath + "mesh.frag");
    turtle_shader.enable_block(shader_block::lights);
    turtle_shader.enable_block(shader_block::material);
    turtle_shader.enable_block(shader_block::matrices);
    // Position.
    m_turtle.set_position(glm::vec3(5.f, 10.f, 5.f));
    // Material.
    turtle_mesh.material.diffuse = glm::vec4(0.f, 0.33f, 1.f, 0.f);

    // Load the monkey.
    mesh& monkey_mesh = m_monkey.internal_mesh();
    monkey_mesh.load_from_disk(path::MeshPath + "monkey.obj");
    monkey_mesh.material.diffuse = glm::vec4(1.0, 0.0, 0.0, 1.0);
    m_monkey.set_position(glm::vec3(40.f, 10.f, 40.f));
    m_monkey.set_scale(glm::vec3(2.f, 2.f, 2.f));
    m_monkey.set_rotation(90.f, glm::vec3(0.f, -1.f, 0.f));

    // Configure the reflection.
    m_reflect.add_mesh(&m_teapot);
    m_reflect.set_texture(m_sky.sky_texture());
    // Configure the toon.
    m_toon.add_mesh(&m_monkey);

    // Configure the water.
    m_water.set_size(m_terrain.size() / 2.f);
    m_water.set_altitude(-3.f);
  }

  void
  scene::final_pass()
  {
    // Configure the heightmap.
    m_terrain.internal_shader().use();
    m_terrain.internal_shader().send_bool("in_enable_clipping", false);
    // Render the objects.
    render_objects();
    // Render the water.
    glm::vec3 p = m_camera.position();
    m_water.internal_shader().send_vec3("in_cp", p.x, p.y, p.z);
    m_water.draw(m_engine);
    // Must be done last.
    m_forest.draw(m_engine);
  }

  void
  scene::render_objects()
  {
    // Clear.
    m_engine.clear();

    // Will update the light.
    m_engine.prepare_rendering();

    // Draw the sky.
    m_engine.disable_depth_buffer();
    m_sky.draw(m_engine);
    m_engine.enable_depth_buffer();

    // Display the different objects.
    m_terrain.draw(m_engine);
    m_turtle.draw(m_engine);
    m_sun.draw(m_engine);
    
    // Disable culling for effects.
    m_reflect.draw(m_engine);
    m_toon.draw(m_engine);
  }

  void
  scene::water_pass()
  {
    // Configure the target.
    m_water.configure_target(m_water_target, false);
    if (m_water_target.is_complete())
      m_engine.set_target(&m_water_target);

    // Modify the camera.
    m_engine.view.push(m_water.compute_view(m_camera));

    // Configure the heightmap.
    m_terrain.internal_shader().use();
    m_terrain.internal_shader().send_bool("in_enable_clipping", true);
    m_terrain.internal_shader().send_bool("in_clip_bottom", true);

    // Draw the objects.
    render_objects();

    // Must be done last.
    m_forest.draw(m_engine);

    // Restore the camera.
    m_engine.view.pop();


    // Configure the target.
    m_water.configure_target(m_water_target, true);
    if (m_water_target.is_complete())
      m_engine.set_target(&m_water_target);

    // Configure the heightmap.
    m_terrain.internal_shader().use();
    m_terrain.internal_shader().send_bool("in_enable_clipping", true);
    m_terrain.internal_shader().send_bool("in_clip_bottom", false);

    // Draw just the terrain and the sky.
    m_engine.clear();
    m_engine.prepare_rendering();
    m_engine.disable_depth_buffer();
    m_sky.draw(m_engine);
    m_engine.enable_depth_buffer();
    // Terrain.
    m_terrain.draw(m_engine);

    // Disable the target.
    m_engine.set_target(NULL);
  }
}
