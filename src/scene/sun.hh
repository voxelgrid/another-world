/*
 * core/scene.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_SUN_HH
#define DEF_LO12_SUN_HH

// Includes, project.
#include <common/math.hh>
#include <core/light.hh>
#include <scene/scene-entity.hh>

namespace lo12
{
  // Forward declarations.
  class light;
  class renderer;
  class skybox;

  class sun
    : public scene_entity
  {
    public:
      /// Ctor of sun.
      sun();

      /// Draw the sun.
      void draw(renderer&);

      /// Set the sun's light.
      void set_light(light*);

      /// Set the sky.
      void set_sky(skybox*);

      /// Update the sun.
      void update(float);

    private:
      light*    m_light;
      skybox*   m_sky;
      glm::vec4 m_position;
  };
}

#endif /* end of include guard: DEF_LO12_SUN_HH */
