/*
 * scene/forest.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_FOREST_HH
#define DEF_LO12_FOREST_HH

// Includes, project.
#include <core/texture2d.hh>
#include <scene/scene-entity.hh>
// Includes, standard.
#include <cstdint>
#include <string>

namespace lo12
{
  // Forward declarations.
  class renderer;

  class forest
    : public scene_entity
  {
    public:
      /// Ctor of forest.
      forest();

      /// Add a tree.
      void add_tree(glm::vec3 const&);

      /// Draw the trees.
      void draw(renderer&);

      /// Remove a tree.
      void remove(size_t);

      /// Get the number of tree.
      size_t size() const;
  
    private:
      // Tree's positions.
      std::vector<glm::vec3> m_positions;
      // Tree's texture.
      texture2d m_texture;
  };
}

#endif /* end of include guard: DEF_LO12_FOREST_HH */
