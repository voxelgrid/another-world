/*
 * common/config.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_CONFIG_HH
#define DEF_LO12_CONFIG_HH

namespace lo12
{
  namespace config
  {
    // Viewport dimension.
    size_t const ViewportWidth  = 800;
    size_t const ViewportHeight = 600;
  }
}

#endif /* end of include guard: DEF_LO12_CONFIG_HH */
