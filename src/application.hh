/*
 * application.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_APPLICATION_HH
#define DEF_LO12_APPLICATION_HH

// Includes, project.
#include <core/light.hh>
#include <scene/scene.hh>
#include <tools/glew-handler.hh>
// Includes, sfml.
#include <SFML/Window.hpp>

namespace lo12
{
  class application
  {
    public:
      /// Ctor of application.
      application(int argc, char** argv);

      /// Dtor of application.
      ~application();

      /// Run the application.
      void run();

    private:
      /// Initialize the application.
      void initialize();

      /// Initialize the lights.
      void initialize_lights();

      /// Update the application.
      void update();

      /// Update the camera.
      void update_camera(float);

    private:
      // OpenGL context.
      sf::Window       m_window;    ///< Windows.
      sf::Input const& m_input;     ///< Input handler.
      glew_handler&    m_extension; ///< Glew access point.

      // Scene.
      scene     m_scene;
      renderer& m_engine;

      // Camera.
      camera&     m_camera;
      move_state& m_state;

      // Lights.
      std::vector<light*> m_lights;

      // Mesh.
      scene_mesh m_mesh;
  };
}

#endif /* end of include guard: DEF_LO12_APPLICATION_HH */

