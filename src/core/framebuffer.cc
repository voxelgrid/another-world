/*
 * core/framebuffer.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <common/debug.hh>
#include <core/framebuffer.hh>
#include <core/texture2d.hh>
// Includes, standard.
#include <cassert>
#include <stdexcept>

namespace lo12
{
  // Invalid object ID.
  GLuint const InvalidID = 0;

  void
  framebuffer::no_target()
  {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }

  framebuffer::framebuffer()
    : m_id(0)
  {
    // Make a buffer.
    glGenFramebuffers(1, &m_id);
    if (m_id == InvalidID)
      throw std::runtime_error("cannot create fbo");
    // Bind to finish the creation.
    bind();

    // Provide a depth buffer.
    GLuint rbuf_id;
    glGenRenderbuffers(1, &rbuf_id);
    glBindRenderbuffer(GL_RENDERBUFFER, rbuf_id);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, 800, 600);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbuf_id);
  }

  framebuffer::~framebuffer()
  {
    if (m_id != InvalidID)
      glDeleteFramebuffers(1, &m_id);
  }

  void
  framebuffer::attach(framebuffer::slot attachment, texture2d const& tex)
  {
    // Binding.
    bind();
    tex.bind();
    // Attach.
    glFramebufferTexture2D(GL_FRAMEBUFFER, attachment,
                           GL_TEXTURE_2D, tex.id(), 0);
    // Update internal state.
    m_attached.push_back(attachment);
  }

  framebuffer::slot_array const&
  framebuffer::attached_slots() const
  {
    return m_attached;
  }

  void
  framebuffer::bind() const
  {
    glBindFramebuffer(GL_FRAMEBUFFER, m_id);
  }

  bool
  framebuffer::is_complete(bool can_warn) const
  {
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE && can_warn)
    {
      switch (status)
      {
        case GL_FRAMEBUFFER_UNDEFINED:
          lo12_warn("fbo undefined, no window exists ?");
          break;
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
          lo12_warn("fbo incomplete attachment");
          break;
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
          lo12_warn("fbo missing attachment");
          break;
        case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
          lo12_warn("fbo incomplete draw buffer");
          break;
        case GL_FRAMEBUFFER_UNSUPPORTED:
          lo12_warn("fbo unsupported, reconsder the format of attached buffers");
          break;
        default:;
      }
    }

    return status == GL_FRAMEBUFFER_COMPLETE;
  }
}
