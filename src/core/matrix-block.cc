/*
 * core/matrix-block.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <core/matrix-block.hh>
#include <core/semantic.hh>

namespace lo12
{
  matrix_block::matrix_block()
    : uniform_buffer()
    , data()
  {
    resize(sizeof(matrix_data));
  }

  void
  matrix_block::bind() const
  {
    uniform_buffer::bind();
    glBindBufferRange(GL_UNIFORM_BUFFER, shader_block::matrices_slot,
                      id(), 0, sizeof(matrix_data));
  }

  void
  matrix_block::send()
  {
    upload(&data, 0, 1);
  }
}
