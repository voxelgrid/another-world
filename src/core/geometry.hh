/*
 * core/geometry.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_GEOMETRY_HH
#define DEF_LO12_GEOMETRY_HH

// Includes, project.
#include <common/opengl.hh>
#include <core/generic-buffer.hh>
#include <core/semantic.hh>
// Includes, boost.
// Includes, standard.
#include <cstdint>
#include <memory>
#include <vector>

namespace lo12
{
  namespace topology
  {
    enum value
    {
      triangles      = GL_TRIANGLES,
      triangle_strip = GL_TRIANGLE_STRIP
    };
  }

  class geometry
  {
    // Typedefs.
    typedef std::shared_ptr<vertex_buffer> buffer_ptr;
    typedef std::vector<buffer_ptr>        buffer_array;

    public:
      /// Ctor of geometry.
      geometry();

      /// Check if an array can be uploaded.
      template <typename T>
      bool can_upload(vertex_attribute::layout, T const*, size_t) const;

      /// Disable the attribute arrays.
      void disable() const;

      /// Enable the attribute arrays.
      void enable() const;

      /// Get the index buffer.
      index_buffer& indices();

      /// Get the index buffer (const access).
      index_buffer const& indices() const;

      /// Resize an attribute array.
      void resize(vertex_attribute::layout, size_t);

      /// Send the geometry.
      void send() const;

      /// Set the topology.
      void set_topology(topology::value);

      /// Get an attribute array' size.
      size_t size(vertex_attribute::layout) const;

      /// Get the topology.
      topology::value topology() const;

      /// Upload a whole array.
      template <typename T>
      void upload(vertex_attribute::layout, T const*, size_t);

      /// Get the layout hash.
      uint32_t layout_hash() const;

    private:
      /// Check if an attribute array is created.
      bool already_created(vertex_attribute::layout) const;

      /// Create an attribute array.
      void create_array(vertex_attribute::layout);

      /// Bind locally an attribute array.
      void local_bind(vertex_attribute::layout) const;

      /// Update the hash.
      void update_hash();

    private:
      buffer_array    m_vbo;
      index_buffer    m_ibo;
      topology::value m_topology;
      uint32_t        m_hash;
  };
}

// Includes, implementation.
#include <core/geometry.hxx>

#endif /* end of include guard: DEF_LO12_GEOMETRY_HH */
