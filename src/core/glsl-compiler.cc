/*
 * core/glsl-compiler.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <core/glsl-compiler.hh>

namespace lo12
{
  glsl_compiler::glsl_compiler()
  {}

  glsl_compiler&
  glsl_compiler::get()
  {
    static glsl_compiler s_instance;
    return s_instance;
  }

  std::string
  glsl_compiler::compile(GLuint stage) const
  {
    // Driver's compiler.
    glCompileShader(stage);

    // Check the compiler status.
    GLint success = GL_FALSE;
    glGetShaderiv(stage, GL_COMPILE_STATUS, &success);
    // Get the compiler message.
    std::string msg = last_compiler_msg(stage);

    if (success == GL_FALSE)
      throw std::runtime_error("shader compilation failed:\n" + msg);
    return msg;
  }

  std::string
  glsl_compiler::link(GLuint program) const
  {
    // Link the shader.
    glLinkProgram(program);
    // Get the linker status.
    GLint success = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    // Get the linker message.
    std::string msg = last_linker_msg(program);

    if (success == GL_FALSE)
      throw std::runtime_error("shader link failed:\n" + msg);
    return msg;
  }

  std::string
  glsl_compiler::last_compiler_msg(GLuint stage) const
  {
    // Get the length of the message.
    GLint length = 0;
    glGetShaderiv(stage, GL_INFO_LOG_LENGTH, &length);
    // Make a buffer.
    std::string buffer(length, ' ');
    // Read the message.
    glGetShaderInfoLog(stage, length, NULL, &buffer[0]);

    return buffer;
  }

  std::string
  glsl_compiler::last_linker_msg(GLuint program) const
  {
    // Get the length of the message.
    GLint length = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
    // Make a buffer.
    std::string buffer(length, ' ');
    // Read the message.
    glGetProgramInfoLog(program, length, NULL, &buffer[0]);

    return buffer;
  }
}
