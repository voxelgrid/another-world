/*
 * core/renderer.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <core/mesh.hh>
#include <core/framebuffer.hh>
#include <core/shader.hh>
#include <core/texture.hh>
#include <core/renderer.hh>
// Includes, standard.
#include <iostream>

namespace lo12
{
  renderer::renderer()
    : projection()
    , model()
    , view()
    , m_target(NULL)
    , m_lights()
    , m_material()
    , m_matrices()
  {
    // Clear color.
    glClearColor(0.4f, 0.7f, 0.3f, 0.f);

    // Provide a depth buffer.
    glEnable(GL_DEPTH_TEST);
    // Enable culling.
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
  }

  renderer::~renderer()
  {}

  void
  renderer::clear()
  {
    // Clear the buffers.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  }

  void
  renderer::disable_culling()
  {
    glDisable(GL_CULL_FACE);
  }

  void
  renderer::disable_depth_buffer()
  {
    glDepthMask(GL_FALSE);
  }

  void
  renderer::enable_culling()
  {
    glEnable(GL_CULL_FACE);
  }

  void
  renderer::enable_depth_buffer()
  {
    glDepthMask(GL_TRUE);
  }

  light*
  renderer::get_light(size_t slot)
  {
    return &m_lights.data[slot];
  }
  
  void
  renderer::prepare_rendering()
  {
    // Send the light.
    m_lights.bind();
    m_lights.send();
  }

  void
  renderer::process(shader& kernel, mesh const& entity)
  {
    // Compare the layout hash.
    if (kernel.layout_hash() != entity.geometry.layout_hash())
      throw std::runtime_error("shader and geometry hashes are different");

    // Matrices.
    kernel.use();
    kernel.bind_block(shader_block::matrices, shader_block::matrices_slot);
    send_matrices();

    // Material.
    if (kernel.block_enabled(shader_block::material))
    {
      kernel.bind_block(shader_block::material, shader_block::material_slot);
      m_material.data = entity.material;
      m_material.bind();
      m_material.send();
    }

    // Lighting.
    if (kernel.block_enabled(shader_block::lights))
    {
      kernel.bind_block(shader_block::lights, shader_block::lights_slot);
      m_lights.bind();
    }

    // Send the geometry.
    entity.geometry.enable();
    entity.geometry.send();
    
    // Get the ibo.
    index_buffer const& ibo = entity.geometry.indices();

    // Number of indices.
    size_t nb_index = ibo.size_in_byte() / sizeof(size_t);
    // Rendering!
    ibo.bind();
    glDrawElements(entity.geometry.topology(), nb_index, GL_UNSIGNED_INT, NULL);

    // Disable the array.
    entity.geometry.disable();
  }

  void
  renderer::set_target(framebuffer const* target)
  {
    m_target = target;
    if (m_target)
      m_target->bind();
    else
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }

  void
  renderer::set_texture(texture_slot::value slot, texture const& tex)
  {
    glActiveTexture(texture_slot::token[slot]);
    tex.bind();
  }

  void
  renderer::send_matrices()
  {
    // Update the matrices.
    m_matrices.data.projection   = projection.top();
    m_matrices.data.modelview    = view.top() * model.top();
    m_matrices.data.normal       = glm::inverse(glm::transpose(view.top() * model.top()));
    m_matrices.data.mvp          = projection.top() * m_matrices.data.modelview;
    m_matrices.data.view         = view.top();
    m_matrices.data.inverse_view = glm::inverse(view.top());
    m_matrices.data.model        = model.top();

    // Enable the block.
    m_matrices.bind();
    // Send the matrices.
    m_matrices.send();
  }
}
