/*
 * core/cubemap.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <common/opengl.hh>
#include <core/cubemap.hh>
#include <tools/image-loader.hh>
// Includes, standard.
#include <cstdint>
#include <vector>

namespace lo12
{
  // Number of face in a cubemap.
  size_t const NbFace = 6;
  // Cubemap tokens.
  static GLenum const CubemapToken[NbFace] =
  {
    GL_TEXTURE_CUBE_MAP_POSITIVE_X,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
  };

  cubemap::cubemap()
    : texture(GL_TEXTURE_CUBE_MAP)
  {}

  void
  cubemap::load_from_disk(std::string const& posx, std::string const& negx,
                          std::string const& posy, std::string const& negy,
                          std::string const& posz, std::string const& negz)
  {
    // Compose the array of filenames.
    std::string const names[NbFace] = {
      posx, negx, posy, negy, posz, negz
    };

    // Configuration.
    set_mag_filter(filter_mode::linear_mipmap);
    set_min_filter(filter_mode::linear_mipmap);
    set_wrap(wrap_mode::clamp_to_edge);

    image_array pixels;
    size_t width  = 0;
    size_t height = 0;
    size_t bpp    = 0;

    // Load each faces.
    static image_loader loader;
    for (size_t i = 0; i < NbFace; ++i)
    {
      // Load the image.
      loader.load_from_disk(names[i], pixels, width, height, bpp);
      // Upload the data.
      glTexImage2D(CubemapToken[i], 0, GL_RGBA, width, height,
                   0, GL_RGBA, GL_UNSIGNED_BYTE, &pixels[0]);
    }

    // Generate mipmap.
    glGenerateMipmap(m_target);
  }
}
