/*
 * core/matrix-block.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_MATRIX_BLOCK_HH
#define DEF_LO12_MATRIX_BLOCK_HH

// Includes, project.
#include <common/math.hh>
#include <core/generic-buffer.hh>

namespace lo12
{
  struct matrix_data
  {
    glm::mat4 projection;
    glm::mat4 modelview;
    glm::mat4 mvp;
    glm::mat4 view;
    glm::mat4 inverse_view;
    glm::mat4 normal;
    glm::mat4 model;
  };

  class matrix_block
    : public uniform_buffer
  {
    public:
      matrix_data data;

    public:
      /// Ctor of matrix_block.
      matrix_block();

      /// Bind the block.
      void bind() const;

      /// Send the current matrices.
      void send();
  };
}

#endif /* end of include guard: DEF_LO12_MATRIX_BLOCK_HH */
