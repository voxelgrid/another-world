/*
 * core/renderer.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_RENDERER_HH
#define DEF_LO12_RENDERER_HH

// Includes, project.
#include <core/semantic.hh>
#include <core/light-block.hh>
#include <core/matrix-block.hh>
#include <core/material-block.hh>
#include <tools/matrix-stack.hh>

namespace lo12
{
  // Forward declaration.
  class light;
  class mesh;
  class framebuffer;
  class shader;
  class texture;

  class renderer
  {
    public:
      matrix_stack projection;
      matrix_stack model;
      matrix_stack view;

    public:
      /// Ctor of renderer.
      renderer();
      
      /// Dtor of renderer.
      ~renderer();

      /// Clear the buffers.
      void clear();

      /// Disable the culling.
      void disable_culling();

      /// Disable the depth buffer.
      void disable_depth_buffer();

      /// Enable the culling.
      void enable_culling();

      /// Enable the depth buffer.
      void enable_depth_buffer();

      /// Get a light.
      light* get_light(size_t);

      /// Prepare the rendering.
      void prepare_rendering();

      /// Process a rendering.
      void process(shader&, mesh const&);

      /// Set the rendertarget.
      void set_target(framebuffer const* target);

      /// Set a texture on a slot.
      void set_texture(texture_slot::value, texture const&);

    private:
      /// Send the matrices.
      void send_matrices();

    private:
      // Render Target.
      framebuffer const* m_target;
      // UBOs.
      light_block    m_lights;
      material_block m_material;
      matrix_block   m_matrices;
  };
}

#endif /* end of include guard: DEF_LO12_RENDERER_HH */
