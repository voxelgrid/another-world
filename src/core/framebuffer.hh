/*
 * core/framebuffer.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_FRAMEBUFFER_HH
#define DEF_LO12_FRAMEBUFFER_HH

// Includes, project.
#include <common/opengl.hh>
// Includes, standard.
#include <vector>

namespace lo12
{
  // Forward declarations.
  class texture2d;

  class framebuffer
  {
    public:
      // Attach point.
      enum slot
      {
        color0_slot = GL_COLOR_ATTACHMENT0,
        color1_slot = GL_COLOR_ATTACHMENT1,
        color2_slot = GL_COLOR_ATTACHMENT2
      };

      // Useful typedef.
      typedef std::vector<slot> slot_array;

      // No target.
      static void no_target();

    public:
      /// Ctor of framebuffer.
      framebuffer();

      /// Dtor of framebuffer.
      ~framebuffer();

      /// Attach a texture2d.
      void attach(slot, texture2d const&);

      /// Get the attached slots.
      slot_array const& attached_slots() const;

      /// Bind the framebuffer.
      void bind() const;

      /// Check the completness of the framebuffer.
      bool is_complete(bool = true) const;

    private:
      // OpenGL ID.
      GLuint m_id;
      // Slot in use.
      slot_array m_attached;
  };
}

#endif /* end of include guard: DEF_LO12_FRAMEBUFFER_HH */
