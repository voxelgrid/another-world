/*
 * core/mesh.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <common/debug.hh>
#include <core/mesh.hh>
// Includes, assimp.
#include <assimp/assimp.hpp>
#include <assimp/aiPostProcess.h>
#include <assimp/aiScene.h>
// Includes, standard.
#include <fstream>
#include <stdexcept>
#include <vector>

namespace lo12
{
  // Assimp's loading flags.
  unsigned int AssimpFlags = aiProcessPreset_TargetRealtime_Fast
                           | aiProcess_GenSmoothNormals;

  mesh::mesh()
    : geometry()
    , m_use_texcoord(false)
  {}

  mesh::~mesh()
  {}

  void
  mesh::load_from_disk(std::string const& filename, int options)
  {
    // Does the file exist ?
    std::ifstream input(filename.c_str());
    if (!input)
      throw std::runtime_error("mesh's file not found: " + filename);

    // Generate the options.
    unsigned load_flags = AssimpFlags;
    // Texcoord.
    m_use_texcoord = options & mesh_option::gen_texcoord;

    // Load the file, the aiScene is handled by the Importer.
    // Each time a mesh is loaded, the previous scene is free.
    static Assimp::Importer importer;
    aiScene const* scene = importer.ReadFile(filename, load_flags);

    if (scene->mNumMeshes == 0)
      throw std::runtime_error("any mesh found in: " + filename);

    // Warn if more than one mesh, error if no mesh.
    if (scene->mNumMeshes > 1)
      lo12_warn("there are more than one mesh in: " << filename);
    // Parse the scene and feed the geometry.
    read_from_assimp(scene->mMeshes[0]);
  }

  void
  mesh::read_from_assimp(aiMesh const* assimp_mesh)
  {
    size_t face_index   = 0;
    size_t nb_vertices  = assimp_mesh->mNumVertices;
    size_t nb_face      = assimp_mesh->mNumFaces;
    size_t size_in_byte = 0;

    aiVector3D const* position = assimp_mesh->mVertices;
    aiVector3D const* normal   = assimp_mesh->mNormals;
    aiVector3D const* texcoord = assimp_mesh->mTextureCoords[0];

    // Upload position.
    if (assimp_mesh->HasPositions())
    {
      size_in_byte = nb_vertices * sizeof(aiVector3D);
      geometry.resize(vertex_attribute::position, size_in_byte);
      geometry.upload(vertex_attribute::position, position, nb_vertices);
    }

    // Upload normal.
    if (assimp_mesh->HasNormals())
    {
      size_in_byte = nb_vertices * sizeof(aiVector3D);
      geometry.resize(vertex_attribute::normal, size_in_byte);
      geometry.upload(vertex_attribute::normal, normal, nb_vertices);
    }

    // Upload texcoord.
    if (assimp_mesh->HasTextureCoords(0) && m_use_texcoord)
    {
      // First, build the new array.
      std::vector<aiVector2D> raw_texcoord(nb_vertices);
      for (size_t i = 0; i < raw_texcoord.size(); ++i)
      {
       raw_texcoord[i].x = texcoord[i].x;
       raw_texcoord[i].y = texcoord[i].y;
      }

      size_in_byte = nb_vertices * sizeof(aiVector2D);
      geometry.resize(vertex_attribute::texcoord0, size_in_byte);
      geometry.upload(vertex_attribute::texcoord0, &raw_texcoord[0], nb_vertices);
    }

    // How many faces ?
    nb_face = assimp_mesh->mNumFaces;
    std::vector<size_t> face(nb_face * 3);

    // Get the faces.
    for (size_t i = 0; i < nb_face; ++i)
    {
      aiFace const* tmp = &assimp_mesh->mFaces[i];
      memcpy(&face[face_index], tmp->mIndices, 3*sizeof(float));
      face_index += 3;
    }

    // Upload face.
    geometry.indices().resize(face.size() * sizeof(size_t));
    geometry.indices().upload(&face[0], 0, nb_face * 3);
  }
}
