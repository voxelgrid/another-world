/*
 * core/geometry.hxx
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_GEOMETRY_HXX
#define DEF_LO12_GEOMETRY_HXX

// Includes, standard.
#include <stdexcept>

namespace lo12
{
  template <typename T>
  bool
  geometry::can_upload(vertex_attribute::layout attribute,
                       T const* data, size_t nb_element) const
  {
    bool is_created = already_created(attribute);
    bool has_memory = size(attribute) == nb_element * sizeof(T);

    return is_created && has_memory;
  }

  template <typename T>
  void
  geometry::upload(vertex_attribute::layout attribute,
                   T const* data, size_t nb_element)
  {
    if (!already_created(attribute))
      create_array(attribute);
    if (!can_upload(attribute, data, nb_element))
      throw std::runtime_error("cannot upload data in the geometry");

    local_bind(attribute);
    m_vbo[attribute]->upload(data, 0, nb_element);
  }
}

#endif /* end of include guard: DEF_LO12_GEOMETRY_HXX */
