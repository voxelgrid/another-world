/*
 * core/shader.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_SHADER_HH
#define DEF_LO12_SHADER_HH

// Includes, project.
#include <common/opengl.hh>
#include <core/semantic.hh>
// Includes, standard.
#include <cstdint>
#include <map>
#include <string>

namespace lo12
{
  namespace shader_stage
  {
    // Internal name.
    enum value
    {
      vertex   = 0,
      fragment = 1,
      nb_max   = 2
    };

    // OpenGL's token.
    GLenum const token[nb_max] = {
      GL_VERTEX_SHADER,
      GL_FRAGMENT_SHADER
    };
  }

  class shader
  {
    // Typedefs.
    typedef std::map<std::string, GLint> uniform_table;

    public:
      /// Ctor of shader.
      shader();

      /// Dtor of shader.
      ~shader();

      /// Bind a uniform block to a slot.
      void bind_block(std::string const&, size_t) const;

      /// Is a block enabled ?
      bool block_enabled(std::string const&) const;

      /// Enable a uniform block.
      void enable_block(std::string const&);

      /// Get the layout hash.
      uint32_t layout_hash() const;

      /// Load from disk.
      void load_from_disk(std::string const&, std::string const&);

      /// Send a boolean.
      void send_bool(std::string const&, bool);

      /// Send a float.
      void send_float(std::string const&, float);

      /// Send a mat4.
      void send_mat4(std::string const&, float const*);
      
      /// Send a texture slot.
      void send_tex(std::string const&, texture_slot::value);

      /// Send a vec3.
      void send_vec3(std::string const&, float, float, float);

      /// Send a vec4.
      void send_vec4(std::string const&, float, float, float, float);

      /// Use the shader.
      void use() const;

    private:
      /// Find a uniform's index.
      bool find_uniform(std::string const&);

      /// Find a uniform block's index.
      bool find_uniform_block(std::string const&);

      /// Link the stages.
      void link() const;

      /// Load and compile a source.
      void load_and_compile(shader_stage::value, std::string const&);

    private:
      GLuint        m_stage[shader_stage::nb_max];
      GLuint        m_program;
      uint32_t      m_hash;
      uniform_table m_uniforms;
  };
}

#endif /* end of include guard: DEF_LO12_SHADER_HH */
