/*
 * core/generic-buffer.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_GENERIC_BUFFER_HH
#define DEF_LO12_GENERIC_BUFFER_HH

// Includes, project.
#include <common/opengl.hh>
// Includes, standard.
#include <cassert>
#include <cstdint>

namespace lo12
{
  namespace buffer_type
  {
    enum value
    {
      ibo = GL_ELEMENT_ARRAY_BUFFER,
      ubo = GL_UNIFORM_BUFFER,
      vbo = GL_ARRAY_BUFFER
    };
  }

  namespace buffer_usage
  {
    enum value
    {
      static_copy = GL_STATIC_COPY,
      static_draw = GL_STATIC_DRAW,
      stream_copy = GL_STREAM_COPY,
      stream_draw = GL_STREAM_DRAW
    };
  }

  template <buffer_type::value type>
  class generic_buffer
  {
    public:
      /// Ctor of generic_buffer.
      generic_buffer(buffer_usage::value = buffer_usage::stream_draw);

      /// Dtor of generic_buffer.
      virtual
      ~generic_buffer();

      /// Bind the buffer.
      void bind() const;

      /// Get the OpenGL ID.
      GLuint id() const;

      /// Get the buffer' size.
      size_t size_in_byte() const;

      /// Resize the buffer.
      void resize(size_t);

      /// Set a new usage.
      void set_usage(buffer_usage::value);

      /// Upload an array in the buffer.
      template <typename T>
      void upload(T const*, size_t, size_t, size_t = 0);

      /// Upload a raw array in the buffer.
      void upload_raw(uint8_t const*, size_t, size_t, size_t = 0);

    private:
      /// Get the buffer' size in number of T.
      template <typename T>
      size_t size_in() const;

    private:
      GLuint m_id;
      size_t m_size;
      buffer_usage::value m_usage;
  };

  // Useful typedef.
  typedef generic_buffer<buffer_type::vbo> vertex_buffer;
  typedef generic_buffer<buffer_type::ibo> index_buffer;
  typedef generic_buffer<buffer_type::ubo> uniform_buffer;
}

// Includes, implementation.
#include <core/generic-buffer.hxx>

#endif /* end of include guard: DEF_LO12_GENERIC_BUFFER_HH */
