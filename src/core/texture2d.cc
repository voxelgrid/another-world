/*
 * core/texture2d.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <core/texture2d.hh>
#include <tools/image-loader.hh>

namespace lo12
{
  texture2d::texture2d()
    : texture(GL_TEXTURE_2D)
  {}

  void
  texture2d::allocate(texture_format::value fmt, size_t width, size_t height)
  {
    // Do not forget to bind.
    bind();

    // Filter configuration.
    set_mag_filter(filter_mode::linear);
    set_min_filter(filter_mode::linear);
    set_wrap(wrap_mode::clamp_to_edge);

    // Send the data.
    glTexImage2D(GL_TEXTURE_2D, 0, fmt, width, height,
                 0, fmt, GL_UNSIGNED_BYTE, NULL);
    glGenerateMipmap(GL_TEXTURE_2D);
  }

  void
  texture2d::load_from_disk(std::string const& filename)
  {
    image_array pixels;
    size_t width  = 0;
    size_t height = 0;
    size_t bpp    = 0;

    static image_loader loader;
    loader.load_from_disk(filename, pixels, width, height, bpp);

    // Configuration.
    set_mag_filter(filter_mode::linear);
    set_min_filter(filter_mode::linear);
    set_wrap(wrap_mode::clamp_to_edge);

    // Send the data.
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, &pixels[0]);
  }
}
