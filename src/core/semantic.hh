/*
 * core/semantic.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_SEMANTIC_HH
#define DEF_LO12_SEMANTIC_HH

// Includes, project.
#include <common/opengl.hh>
// Includes, standard.
#include <cstdint>
#include <string>

namespace lo12
{
  namespace light_info
  {
    size_t const nb_max = 4;
  }

  namespace vertex_attribute
  {
    enum layout
    {
      position  = 0,
      normal    = 1,
      texcoord0 = 2,
      nb_max    = 3
    };

    uint32_t const hash_value[nb_max] = {
      1 << 0,    // Position hash.
      1 << 1,    // Normal   hash.
      1 << 2     // Texcoord hash.
    };

    size_t const size[nb_max] = {
      3,
      3,
      2
    };

    GLenum const type[nb_max] = {
      GL_FLOAT,
      GL_FLOAT,
      GL_FLOAT
    };
  }

  namespace shader_block
  {
    std::string const lights   = "Lights";
    std::string const material = "Material";
    std::string const matrices = "Matrices";

    GLuint const lights_slot   = 0;
    GLuint const matrices_slot = 1;
    GLuint const material_slot = 2;
  }

  namespace texture_slot
  {
    enum value
    {
      texture0  = 0,
      texture1  = 1,
      skybox    = 2,
      normalmap = 3,
      nb_max    = 4
    };

    GLenum const token[4] = {
      GL_TEXTURE0,
      GL_TEXTURE1,
      GL_TEXTURE2,
      GL_TEXTURE3
    };
  }
}

#endif /* end of include guard: DEF_LO12_SEMANTIC_HH */
