/*
 * core/light.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_LIGHT_HH
#define DEF_LO12_LIGHT_HH

// Includes, project.
#include <common/math.hh>
// Includes, standard.
#include <cstdint>
#include <memory>

namespace lo12
{
  class light
  {
    public:
      glm::vec4 position;
      glm::vec4 ambient;
      glm::vec4 diffuse;
      glm::vec4 specular;
      glm::vec4 spot_direction;
      float     constant_attenuation;
      float     linear_attenuation;
      float     quadratic_attenuation;
      float     spot_cutoff;
      float     spot_exponent;
      uint32_t  enabled;
      float     padding[2];

    public:
      /// Ctor of light.
      light()
        : position(0.f, 0.f, 0.f, 0.f)
        , ambient(1.f, 0.f, 0.f, 0.f)
        , diffuse(2.f, 0.f, 0.f, 0.f)
        , specular(3.f, 0.f, 0.f, 0.f)
        , spot_direction(0.0, -2.0, 0.0, 0.0)
        , constant_attenuation(0.3f)
        , linear_attenuation(0.2f)
        , quadratic_attenuation(0.f)
        , spot_cutoff(0.f)
        , spot_exponent(0.f)
        , enabled(false)
      {}
  };
}

#endif /* end of include guard: DEF_LO12_LIGHT_HH */
