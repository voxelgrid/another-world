/*
 * core/geometry.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <core/geometry.hh>

namespace lo12
{
  geometry::geometry()
    : m_vbo(vertex_attribute::nb_max)
    , m_ibo()
    , m_topology(topology::triangles)
    , m_hash(0)
  {
    for (size_t i = 0; i < vertex_attribute::nb_max; ++i)
      m_vbo[i] = NULL;
  }

  void
  geometry::disable() const
  {
    for (size_t i = 0; i < vertex_attribute::nb_max; ++i)
      if (m_vbo[i] != NULL)
        glDisableVertexAttribArray(i);
  }

  void
  geometry::enable() const
  {
    for (size_t i = 0; i < vertex_attribute::nb_max; ++i)
      if (m_vbo[i] != NULL)
        glEnableVertexAttribArray(i);
  }

  index_buffer&
  geometry::indices()
  {
    return m_ibo;
  }

  /// Get the index buffer.
  index_buffer const&
  geometry::indices() const
  {
    return m_ibo;
  }

  void
  geometry::resize(vertex_attribute::layout attribute, size_t new_size)
  {
    if (!already_created(attribute))
      create_array(attribute);

    local_bind(attribute);
    m_vbo[attribute]->resize(new_size);
  }

  size_t
  geometry::size(vertex_attribute::layout attribute) const
  {
    if (m_vbo[attribute])
      return m_vbo[attribute]->size_in_byte();
    else
      return 0;
  }

  uint32_t
  geometry::layout_hash() const
  {
    return m_hash;
  }

  bool
  geometry::already_created(vertex_attribute::layout attribute) const
  {
    return m_vbo[attribute] != NULL;
  }

  void
  geometry::create_array(vertex_attribute::layout attribute)
  {
    m_vbo[attribute] = std::make_shared<vertex_buffer>();
    local_bind(attribute);
    update_hash();
  }

  void
  geometry::local_bind(vertex_attribute::layout attribute) const
  {
    m_vbo[attribute]->bind();
  }

  void
  geometry::send() const
  {
    for (size_t i = 0; i < vertex_attribute::nb_max; ++i)
    {
      if (m_vbo[i] != NULL)
      {
        m_vbo[i]->bind();
        glVertexAttribPointer(i,
                              vertex_attribute::size[i],
                              vertex_attribute::type[i],
                              GL_FALSE,
                              0,
                              NULL);
      }
    }  
  }

  void
  geometry::set_topology(topology::value topology)
  {
    m_topology = topology;
  }

  topology::value
  geometry::topology() const
  {
    return m_topology;
  }

  void
  geometry::update_hash()
  {
    m_hash = 0;
    for (size_t i = 0; i < vertex_attribute::nb_max; ++i)
      if (m_vbo[i] != NULL)
        m_hash |= vertex_attribute::hash_value[i];
  }
}
