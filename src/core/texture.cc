/*
 * core/texture.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <core/texture.hh>
// Includes, standard.
#include <stdexcept>

namespace lo12
{
  texture::texture(GLenum target)
    : m_id(0)
    , m_target(target)
  {
    glGenTextures(1, &m_id);
    if (m_id == 0)
      throw std::runtime_error("cannot create the texture");
  }

  texture::~texture()
  {
    if (m_id != 0)
     glDeleteTextures(1, &m_id);
  }

  void
  texture::bind() const
  {
    glBindTexture(m_target, m_id);
  }

  GLuint
  texture::id() const
  {
    return m_id;
  }

  void
  texture::set_wrap(wrap_mode::value mode)
  {
    bind();
    glTexParameteri(m_target, GL_TEXTURE_WRAP_S, mode);
    glTexParameteri(m_target, GL_TEXTURE_WRAP_T, mode);
  }

  void
  texture::set_min_filter(filter_mode::value mode)
  {
    bind();
    glTexParameteri(m_target, GL_TEXTURE_MIN_FILTER, mode);
  }

  void
  texture::set_mag_filter(filter_mode::value mode)
  {
    bind();
    glTexParameteri(m_target, GL_TEXTURE_MAG_FILTER, mode);
  }
}
