/*
 * core/generic-buffer.hxx
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA
*/

#ifndef DEF_LO12_GENERIC_BUFFER_HXX
#define DEF_LO12_GENERIC_BUFFER_HXX

// Includes, standard.
#include <stdexcept>

namespace lo12
{
  template <buffer_type::value type>
  generic_buffer<type>::generic_buffer(buffer_usage::value usage)
    : m_id(0)
    , m_usage(usage)
    , m_size(0)
  {
    // Make one buffer.
    glGenBuffers(1, &m_id);
    if (m_id == 0)
      throw std::runtime_error("generic buffer creation failed");
    // The first binding gives the buffer's type.
    bind();
  }

  template <buffer_type::value type>
  generic_buffer<type>::~generic_buffer()
  {
    if (m_id != 0)
      glDeleteBuffers(1, &m_id);
  }

  template <buffer_type::value type>
  void
  generic_buffer<type>::bind() const
  {
    glBindBuffer(type, m_id);
  }

  template <buffer_type::value type>
  GLuint
  generic_buffer<type>::id() const
  {
    return m_id;
  }

  template <buffer_type::value type>
  size_t
  generic_buffer<type>::size_in_byte() const
  {
    return m_size;
  }

  template <buffer_type::value type>
  void
  generic_buffer<type>::resize(size_t new_size)
  {
    bind();

    m_size = new_size;
    glBufferData(type, m_size, NULL, m_usage);
  }

  template <buffer_type::value type>
  template <typename T>
  void
  generic_buffer<type>::upload(T const* data, size_t start, size_t last, size_t offset)
  {
    // Check T.
    static_assert(   type == buffer_type::vbo
                  || type == buffer_type::ubo
                  || std::is_same<unsigned int, T>::value,
      "IBO: you must upload [unsigned int] elements.");

    // Check args.
    assert(data != NULL);
    assert(start <= last);
    assert((last - start) <= size_in<T>());

    uint8_t const* raw   = (uint8_t const*) data;
    size_t offset_start  = sizeof(T) * start;
    size_t offset_last   = sizeof(T) * last;
    size_t offset_buffer = sizeof(T) * offset;

    // Upload the data.
    upload_raw(raw, offset_start, offset_last, offset_buffer);
  }

  template <buffer_type::value type>
  void
  generic_buffer<type>::upload_raw(uint8_t const* raw, size_t start, size_t last, size_t offset)
  {
    // Check args.
    assert(raw != NULL);
    assert(start <= last);
    assert((last - start) <= size_in_byte());

    // Bind is mandatory for uploading.
    bind();
    // Upload the data.
    glBufferSubData(type, offset, last - start, raw + start);
  }

  template <buffer_type::value type>
  template <typename T>
  size_t
  generic_buffer<type>::size_in() const
  {
    return m_size / sizeof(T);
  }
}

#endif /* end of include guard: DEF_LO12_GENERIC_BUFFER_HXX */
