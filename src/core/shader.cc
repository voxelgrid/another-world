/*
 * core/shader.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <common/debug.hh>
#include <core/glsl-compiler.hh>
#include <core/glsl-preprocessor.hh>
#include <core/shader.hh>
// Includes, standard.
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>

namespace lo12
{
  // OpenGL invalid ID.
  GLuint const InvalidObjectID  = 0;
  GLint  const InvalidUniformID = -1;

  shader::shader()
    : m_hash(0)
  {
    // Create the different stages.
    for (size_t i = 0; i < shader_stage::nb_max; ++i)
    {
      m_stage[i] = glCreateShader(shader_stage::token[i]);
      if (m_stage[i] == InvalidObjectID)
        throw std::runtime_error("cannot create shader stage");
    }

    // Create the program.
    m_program = glCreateProgram();
    if (m_program == InvalidObjectID)
      throw std::runtime_error("cannot create shader program");
  }

  shader::~shader()
  {
    // Destroy the different stages.
    for (size_t i = 0; i < shader_stage::nb_max; ++i)
    {
      if (m_stage[i] == InvalidObjectID)
        throw std::runtime_error("cannot create shader stage");
    }
  }

  void
  shader::bind_block(std::string const& name, size_t slot) const
  {
    auto it = m_uniforms.find(name);
    if (it != m_uniforms.end())
      glUniformBlockBinding(m_program, it->second, slot);
    else
      lo12_warn("shader can't bound the block: " << name);
  }

  bool
  shader::block_enabled(std::string const& name) const
  {
    return m_uniforms.find(name) != m_uniforms.end();
  }

  void
  shader::enable_block(std::string const& name)
  {
    if (!block_enabled(name) && !find_uniform_block(name))
      throw std::runtime_error("shader cannot enable the block: " + name);
  }

  uint32_t
  shader::layout_hash() const
  {
    return m_hash;
  }

  void
  shader::load_from_disk(std::string const& vertex_filename,
                         std::string const& fragment_filename)
  {
    // Open the vertex file.
    std::ifstream input(vertex_filename.c_str());
    if (!input)
      throw std::runtime_error("vertex file not found: " + vertex_filename);

    // Read the content.
    std::stringstream content;
    content << input.rdbuf();

    // Update the hash.
    m_hash = glsl_preprocessor::get().layout_hash(content.str());

    // Load and compile.
    std::string src = glsl_preprocessor::get().resolve_include(content.str());
    load_and_compile(shader_stage::vertex, src);

    // Open the fragment file.
    input.close();
    input.open(fragment_filename.c_str());
    if (!input)
      throw std::runtime_error("vertex file not found: " + vertex_filename);

    // Read the content.
    content.str("");
    content << input.rdbuf();

    // Load and compile.
    src = glsl_preprocessor::get().resolve_include(content.str());
    load_and_compile(shader_stage::fragment, src);

    // Finish the creation and link the stages.
    link();
  }

  void
  shader::send_bool(std::string const& name, bool value)
  {
    // Looks for the index.
    bool found =  m_uniforms.find(name) != m_uniforms.end()
      || find_uniform(name);
    // The uniform can be send.
    if (found)
      glUniform1i(m_uniforms[name], value);
    else
      lo12_warn("shader can't send the boolean: " << name);
  }

  void
  shader::send_float(std::string const& name, float value)
  {
    // Looks for the index.
    bool found =  m_uniforms.find(name) != m_uniforms.end()
               || find_uniform(name);
    // The uniform can be send.
    if (found)
      glUniform1f(m_uniforms[name], value);
    else
      lo12_warn("shader can't send the float: " << name);
  }

  void
  shader::send_mat4(std::string const& name, float const* matrix)
  {
    // Looks for the index.
    bool found =  m_uniforms.find(name) != m_uniforms.end()
               || find_uniform(name);
    // The uniform can be send.
    if (found)
      glUniformMatrix4fv(m_uniforms[name], 1, GL_FALSE, matrix);
    else
      lo12_warn("shader can't send the mat4: " << name);
  }

  void
  shader::send_tex(std::string const& name, texture_slot::value slot)
  {
    // Looks for the index.
    bool found =  m_uniforms.find(name) != m_uniforms.end()
      || find_uniform(name);
    // The uniform can be send.
    if (found)
      glUniform1i(m_uniforms[name], slot);
    else
      lo12_warn("shader can't send the texture: " << name);
  }

  void
  shader::send_vec3(std::string const& name, float x, float y, float z)
  {
    // Looks for the index.
    bool found =  m_uniforms.find(name) != m_uniforms.end()
               || find_uniform(name);
    // The uniform can be send.
    if (found)
      glUniform3f(m_uniforms[name], x, y, z);
    else
      lo12_warn("shader can't send the vec3: " << name);
  }

  void
  shader::send_vec4(std::string const& name, float x, float y, float z, float w)
  {
    // Looks for the index.
    bool found =  m_uniforms.find(name) != m_uniforms.end()
               || find_uniform(name);
    // The uniform can be send.
    if (found)
      glUniform4f(m_uniforms[name], x, y, z, w);
    else
      lo12_warn("shader can't send the vec4: " << name);
  }

  void
  shader::use() const
  {
    glUseProgram(m_program);
  }

  bool
  shader::find_uniform(std::string const& name)
  {
    GLint index = glGetUniformLocation(m_program, name.c_str());
    if (index != InvalidUniformID)
      m_uniforms[name] = index;

    return index != InvalidUniformID;
  }

  bool
  shader::find_uniform_block(std::string const& name)
  {
    GLint index = glGetUniformBlockIndex(m_program, name.c_str());
    if (index != InvalidUniformID)
      m_uniforms[name] = index;

    return index != InvalidUniformID;
  }

  void
  shader::link() const
  {
    // Attach the stages.
    for (size_t i = 0; i < shader_stage::nb_max; ++i)
      glAttachShader(m_program, m_stage[i]);
    // Invoke the compiler.
    glsl_compiler::get().link(m_program);
  }

  void
  shader::load_and_compile(shader_stage::value stage,
                           std::string const& source)
  {
    // Upload the source.
    char const* src = source.c_str();
    glShaderSource(m_stage[stage], 1, (char const**) &src, NULL);

    // Invoke the compiler.
    glsl_compiler::get().compile(m_stage[stage]);
  }
}
