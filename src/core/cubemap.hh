/*
 * texture/cubemap.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_CUBEMAP_HH
#define DEF_LO12_CUBEMAP_HH

// Includes, project.
#include <core/texture.hh>

namespace lo12
{
  class cubemap
    : public texture
  {
  public:
    /// Ctor of cubemap.
    cubemap();

    /// Load from disk.
    void load_from_disk(std::string const&, std::string const&,
      std::string const&, std::string const&,
      std::string const&, std::string const&);
  };
}

#endif /* end of include guard: DEF_LO12_CUBEMAP_HH */
