/*
 * core/texture.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_TEXTURE_HH
#define DEF_LO12_TEXTURE_HH

// Includes, project.
#include <common/opengl.hh>
// Includes, standard.
#include <cstdint>
#include <string>

namespace lo12
{
  // TexCoord wrap mode.
  namespace wrap_mode
  {
    enum value
    {
      clamp         = GL_CLAMP,
      clamp_to_edge = GL_CLAMP_TO_EDGE,
      repeat        = GL_REPEAT
    };
  }

  // Filter of the texture.
  namespace filter_mode
  {
    enum value
    {
      linear  = GL_LINEAR,
      nearest = GL_NEAREST,
      linear_mipmap  = GL_LINEAR_MIPMAP_LINEAR,
      nearest_mipmap = GL_NEAREST_MIPMAP_NEAREST
    };
  }

  // Format of the data in the texture.
  namespace texture_format
  {
    enum value
    {
      rgb  = GL_RGB,
      rgba = GL_RGBA
    };
  }

  class texture
  {
    public:
      /// Ctor of texture.
      texture(GLenum);

      /// Dtor of texture.
      virtual
      ~texture() = 0;

      /// Bind the texture.
      void bind() const;

      // Get the texture's ID.
      GLuint id() const;

      /// Set the wrap mode.
      void set_wrap(wrap_mode::value);

      /// Set the min mode.
      void set_min_filter(filter_mode::value);

      /// Set the mag filter.
      void set_mag_filter(filter_mode::value);

    protected:
      GLuint m_id;
      GLenum m_target;
  };
}

#endif /* end of include guard: DEF_LO12_TEXTURE_HH */
