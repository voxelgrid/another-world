/*
 * core/glsl-compiler.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_GLSL_COMPILER_HH
#define DEF_LO12_GLSL_COMPILER_HH

// Includes, project.
#include <common/opengl.hh>
// Includes, standard.
#include <string>

namespace lo12
{
  class glsl_compiler
  {
    public:
      /// Get the singleton instance.
      static glsl_compiler& get();

      /// Compile a shader stage.
      std::string compile(GLuint) const;

      /// Link a program.
      std::string link(GLuint) const;

    protected:
      /// Ctor of glsl_compiler.
      glsl_compiler();

    private:
      /// Get the last stage compiler's message.
      std::string last_compiler_msg(GLuint) const;

      /// Get the last program compiler's message.
      std::string last_linker_msg(GLuint) const;
  };
}

#endif /* end of include guard: DEF_LO12_GLSL_COMPILER_HH */
