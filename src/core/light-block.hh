/*
 * core/light-block.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_LIGHT_BLOCK_HH
#define DEF_LO12_LIGHT_BLOCK_HH

// Includes, project.
#include <core/generic-buffer.hh>
#include <core/light.hh>
// Includes, standard.
#include <vector>

namespace lo12
{
  class light_block
    : public uniform_buffer
  {
    public:
      std::vector<light> data;

    public:
      /// Ctor of light_block.
      light_block();

      /// Bind the block.
      void bind() const;

      /// Send the lights.
      void send();
  };
}

#endif /* end of include guard: DEF_LO12_LIGHT_BLOCK_HH */
