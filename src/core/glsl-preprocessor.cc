/*
 * core/glsl-preprocessor.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <common/path.hh>
#include <core/glsl-preprocessor.hh>
#include <core/semantic.hh>
// Includes, boost.
#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>
// Includes, standard.
#include <algorithm>
#include <fstream>
#include <sstream>

namespace lo12
{
  glsl_preprocessor::glsl_preprocessor()
  {}

  glsl_preprocessor&
  glsl_preprocessor::get()
  {
    static glsl_preprocessor s_instance;
    return s_instance;
  }

  uint32_t
  glsl_preprocessor::layout_hash(std::string const& src) const
  {
    static boost::regex const re("^layout\\(location[ ]+=[ ]+([0-9])\\)*");

    std::stringstream input;
    input << src;

    boost::smatch matches;
    std::string   line;
    uint32_t      hash = 0;

    while (std::getline(input, line))
    {
      // Search the layout's ID.
      if (boost::regex_search(line, matches, re))
      {
        // Should throw an exception if the layout is out of range.
        size_t value = boost::lexical_cast<size_t>(matches[1]);
        if (value < vertex_attribute::nb_max)
          hash |= vertex_attribute::hash_value[value];
      }
    }

    return hash;
  }

  std::string
  glsl_preprocessor::resolve_include(std::string const& src, size_t depth) const
  {
    static boost::regex const re("^[ ]*#[ ]*include[ ]+[\"<](.*)[\">].*");

    // End of the recursion.
    if (depth == 0)
      return src;

    std::stringstream input;
    std::stringstream output;
    input << src;

    boost::smatch matches;
    std::string   line;

    while(std::getline(input,line))
    {
      // Search the filename to include.
      if (boost::regex_search(line, matches, re))
      {
        std::string include_file = path::ShaderPath + matches[1];
        std::string include_string;

        std::ifstream file(include_file.c_str());
        if (!file)
          throw std::runtime_error("cannot include: " + include_file);

        std::stringstream file_buffer;
        file_buffer << file.rdbuf();
        
        // Recursion.
        output << resolve_include(file_buffer.str(), depth - 1) << std::endl;
      }
      else
      {
        output <<  line << std::endl;
      }
    }

    return output.str();
  }
}
