/*
 * tools/image-loader.c
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <tools/image-loader.hh>
// Includes, devil.
#include <IL/il.h>
// Includes, standard.
#include <string>
#include <stdexcept>

namespace lo12
{
  image_loader::image_loader()
  {
    static bool initialized = false;
    if (!initialized)
    {
      // Initialize DevIL.
      ilInit();
      // Configure the origin of the images: lower-left
      ilOriginFunc(IL_ORIGIN_UPPER_LEFT);
      ilEnable(IL_ORIGIN_SET);
      // Force loading in RGBA 32.
      ilSetInteger(IL_FORMAT_MODE, IL_RGBA);
      ilEnable(IL_FORMAT_SET);

      // Update the flag.
      initialized = true;
    }
  }

  void
  image_loader::load_from_disk(std::string const& filename, image_array& data,
                               size_t& width, size_t& height, size_t& bpp)
  {
    // Make a DevIL image.
    ILuint texture;
    ilGenImages(1, &texture);
    ilBindImage(texture);

    // Load the image.
    if (!ilLoadImage(const_cast<ILstring>(filename.c_str())))
      throw std::runtime_error("file not found: " + filename);

    // Get image's size.
    width  = ilGetInteger(IL_IMAGE_WIDTH);
    height = ilGetInteger(IL_IMAGE_HEIGHT);
    bpp    = 4;
    // Allocate memory if needed.
    size_t size = bpp * width * height;
    if (data.size() != size)
      data.resize(size);
    // Copy the image.
    uint8_t const* raw = ilGetData();
    std::copy(raw, raw + size, data.begin());

    // Clear DevIL.
    ilBindImage(0);
    ilDeleteImages(1, &texture);
  }
}
