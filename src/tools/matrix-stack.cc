/*
 * tools/matrix-stack.cc
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <tools/matrix-stack.hh>
// Includes, standard.
#include <stdexcept>

namespace lo12
{
  matrix_stack::matrix_stack(size_t depth)
    : m_stack(depth)
    , m_index(0)
  {
    push(glm::mat4(1.f));
  }

  void
  matrix_stack::pop()
  {
    if (m_index == 0)
      throw std::runtime_error("matrix stack must have at least one element");
    m_index--;
  }

  void
  matrix_stack::push()
  {
    push(m_stack[m_index]);
  }

  void
  matrix_stack::push(glm::mat4 const& matrix)
  {
    if ((m_index + 1) == m_stack.size())
      throw std::runtime_error("matrix stack's depth reached");
    m_index++;
    m_stack[m_index] = matrix;
  }

  void
  matrix_stack::rotate(float angle, float x, float y, float z)
  {
    glm::vec3 axis(x, y, z);
    m_stack[m_index] = glm::rotate(m_stack[m_index], angle, axis);
  }

  void
  matrix_stack::scale(float x, float y, float z)
  {
    glm::vec3 axis(x, y, z);
    m_stack[m_index] = glm::scale(m_stack[m_index], axis);
  }

  void
  matrix_stack::set_matrix(glm::mat4 const& matrix)
  {
    m_stack[m_index] = matrix;
  }

  glm::mat4 const&
  matrix_stack::top() const
  {
    return m_stack[m_index];
  }

  void
  matrix_stack::translate(float x, float y, float z)
  {
    glm::vec3 axis(x, y, z);
    m_stack[m_index] = glm::translate(m_stack[m_index], axis);
  }
}
