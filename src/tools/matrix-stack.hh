/*
 * tools/matrix-stack.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

#ifndef DEF_LO12_MATRIX_STACK_HH
#define DEF_LO12_MATRIX_STACK_HH

// Includes, project.
#include <common/math.hh>
// Includes, standard.
#include <vector>

namespace lo12
{
  class matrix_stack
  {
    public:
      /// Ctor of matrix_stack.
      matrix_stack(size_t = 16);

      /// Pop the matrix.
      void pop();

      /// Push the matrix.
      void push();

      /// Push and load the matrix.
      void push(glm::mat4 const&);

      /// Apply a rotation on the current matrix.
      void rotate(float, float, float, float);

      /// Apply a scale on the current matrix.
      void scale(float, float, float);

      /// Set the current matrix.
      void set_matrix(glm::mat4 const&);

      /// Get the top matrix.
      glm::mat4 const& top() const;

      /// Apply a translation on the current matrix.
      void translate(float, float, float);

    private:
      std::vector<glm::mat4> m_stack;
      size_t m_index;
  };
}

#endif /* end of include guard: DEF_LO12_MATRIX_STACK_HH */
