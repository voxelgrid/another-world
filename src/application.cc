/*
 * application.hh
 *
 * Copyright (C) Samuel GOSSELIN
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, 
 * Boston, MA  02110-1301  USA
*/

// Includes, project.
#include <application.hh>
#include <common/path.hh>
// Includes, standard.
#include <iostream>
#include <fstream>
#include <sstream>

namespace lo12
{
  // Window's video mode.
  size_t const WinWidth  = 800;
  size_t const WinHeight = 600;
  size_t const WinBPP    = 32;

  // Window's center.
  int const WinCenterX = WinWidth / 2;
  int const WinCenterY = WinHeight / 2;

  // Window's caption.
  std::string const WinTitle = "+ - Another World - +";

  // Camera configuration.
  float const CameraFovy  = 45.f;
  float const CameraRatio = static_cast<float>(WinWidth) / WinHeight;

  application::application(int argc, char** argv)
    : m_window(sf::VideoMode(WinWidth, WinHeight, WinBPP), WinTitle)
    , m_input(m_window.GetInput())
    , m_extension(glew_handler::get())
    , m_scene()
    , m_engine(m_scene.engine())
    , m_camera(m_scene.get_camera())
    , m_state(m_camera.state())
    , m_mesh()
  {
    // Initialize the application.
    initialize();
  }

  application::~application()
  {}

  void
  application::run()
  {
    sf::Clock clock;
    sf::Event event;
    int time_factor = 1;

    while (m_window.IsOpened())
    {
      while (m_window.GetEvent(event))
      {
        if (m_input.IsKeyDown(sf::Key::Escape))
          m_window.Close();

        // Wire mode.
        if (m_input.IsKeyDown(sf::Key::Space))
          glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        else
          glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        // Lights.
        m_lights[0]->enabled = !m_input.IsKeyDown(sf::Key::Num1);
        m_lights[1]->enabled = !m_input.IsKeyDown(sf::Key::Num2);

        // Time factor.
        time_factor = m_input.IsKeyDown(sf::Key::Y) ? 4 : 1;

        // Torche.
        if (event.Type == sf::Event::KeyPressed
            && m_input.IsKeyDown(sf::Key::T))
        {
          m_lights[2]->enabled = !m_lights[2]->enabled;
        }
        
        // Spot.
        if (m_input.IsKeyDown(sf::Key::P))
          m_lights[1]->spot_cutoff += 2.f;
        if (m_input.IsKeyDown(sf::Key::O))
          m_lights[1]->spot_cutoff -= 2.0f;
      }

      // Update everything.
      update_camera(time_factor * clock.GetElapsedTime());
      m_scene.update(time_factor * clock.GetElapsedTime());
      m_window.SetCursorPosition(WinCenterX, WinCenterY);

      update();

      // Reset the clock.
      clock.Reset();
    }
  }

  void
  application::initialize()
  {
    // Do not display the cursor.
    m_window.ShowMouseCursor(false);
    // Limit the framerate.
    m_window.SetFramerateLimit(50);

    // Configure the camera.
    m_camera.configure_perspective(CameraFovy, CameraRatio, 0.1f, 10000.f);
    m_engine.view.set_matrix(m_camera.view_matrix());
    m_engine.projection.set_matrix(m_camera.perspective_matrix());

    // Speed.
    m_camera.set_sensitivity(550.f);
    m_camera.set_speed(550.f);

    // Initialize the mesh.
    mesh& m   = m_mesh.internal_mesh();
    shader& s = m_mesh.internal_shader();

    // Load it.
    m.load_from_disk(path::MeshPath + "sphere.3ds");
    s.load_from_disk(path::ShaderPath + "sun.vert",
                     path::ShaderPath + "sun.frag");
    s.enable_block(shader_block::material);
    s.enable_block(shader_block::matrices);
    m.material.diffuse = glm::vec4(0.f, 0.f, 1.f, 1.f);

    // Initialize the lights.
    initialize_lights();
  }

  void
  application::initialize_lights()
  {
    // Get the lights.
    for (size_t i = 0; i < light_info::nb_max; ++i)
      m_lights.push_back(m_engine.get_light(i));

    // lights1: spot which rotate around the teapot.
    m_lights[1]->position       = glm::vec4(0.f, 8.f, 0.f, 1.f); 
    m_lights[1]->ambient        = glm::vec4(1.f, 0.f, 1.f, 1.f);
    m_lights[1]->diffuse        = glm::vec4(1.f, 0.f, 1.f, 1.f);
    m_lights[1]->specular       = glm::vec4(1.f, 1.f, 1.f, 1.f);
    m_lights[1]->spot_direction = glm::vec4(0.f, -1.f, 0.f, 0.f);
    m_lights[1]->spot_cutoff    = 45.f;
    m_lights[1]->spot_exponent  = 10.f;
    m_lights[1]->enabled        = true;
    m_lights[1]->linear_attenuation = 0.01f;
    m_lights[1]->quadratic_attenuation = 0.005f;

    // lights2: spot which follow the camera.
    m_lights[2]->position       = glm::vec4(m_camera.position(), 1.f); 
    m_lights[2]->ambient        = glm::vec4(1.f, 1.f, 1.f, 1.f);
    m_lights[2]->diffuse        = glm::vec4(1.f, 1.f, 1.f, 1.f);
    m_lights[2]->specular       = glm::vec4(1.f, 1.f, 1.f, 1.f);
    m_lights[2]->spot_direction = glm::vec4(m_camera.forward(), 0.f);
    m_lights[2]->spot_cutoff    = 45.f;
    m_lights[2]->spot_exponent  = 10.f;
    m_lights[2]->enabled        = false;
    m_lights[2]->constant_attenuation  = 0.3f;
    m_lights[2]->linear_attenuation    = 0.05f;
    m_lights[2]->quadratic_attenuation = 0.f;
  }

  void
  application::update()
  {
    // Update the first spot.
    static float angle = 0.f;
    angle += 1.f;
    m_lights[1]->position.x = 15.f * cos(angle * 3.14f / 180.f);
    m_lights[1]->position.z = 15.f * sin(angle * 3.14f / 180.f);
    m_lights[1]->spot_direction = -glm::normalize(m_lights[1]->position);
    
    m_mesh.set_position(glm::vec3(m_lights[1]->position));

    // Update light2.
    m_lights[2]->position       = glm::vec4(m_camera.position(), 1.f);
    m_lights[2]->spot_direction = glm::vec4(m_camera.forward(), 0.f);

    // Display the scene.
    m_scene.render();

    // Display the light's mesh.
    m_engine.model.set_matrix(glm::mat4(1.f));
    m_mesh.draw(m_engine);
    m_engine.model.set_matrix(glm::mat4(1.f));

    // Refresh the window.
    m_window.Display();
  }

  void
  application::update_camera(float dt)
  {
    m_state.forward      = m_input.IsKeyDown(sf::Key::W);
    m_state.backward     = m_input.IsKeyDown(sf::Key::S);
    m_state.strafe_left  = m_input.IsKeyDown(sf::Key::A);
    m_state.strafe_right = m_input.IsKeyDown(sf::Key::D);

    // Mouse.
    m_state.mouse_dx = m_input.GetMouseX() - WinCenterX;
    m_state.mouse_dy = m_input.GetMouseY() - WinCenterY;

    // Animation.
    m_camera.animate(dt);

    // Update the view matrix.
    m_engine.view.set_matrix(m_camera.view_matrix());
  }
}
